<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Business;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\ContentField\Business\Processor\Update\ContentFieldUpdateProcessorInterface;
use Leonp5\Hybridcms\ContentField\Business\Processor\ContentFieldTableCreate\ContentFieldTableCreateProcessorInterface;

class ContentFieldFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return ContentFieldTableCreateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getContentFieldTableCreateProcessor(): ContentFieldTableCreateProcessorInterface
    {
        return $this->app->make(ContentFieldTableCreateProcessorInterface::class);
    }

    /**
     * @return ContentFieldUpdateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getContentFieldUpdateProcessor(): ContentFieldUpdateProcessorInterface
    {
        return $this->app->make(ContentFieldUpdateProcessorInterface::class);
    }
}
