<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Business;

use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateRequestTransfer;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateResponseTransfer;

interface ContentFieldFacadeInterface
{
    /**
     * @param string $tableName
     * 
     * @return void 
     */
    public function createContentFieldTable(string $tableName): void;

    /**
     * @param ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
     * 
     * @return ContentFieldUpdateResponseTransfer 
     */
    public function updateContentField(
        ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
    ): ContentFieldUpdateResponseTransfer;
}
