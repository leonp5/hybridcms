<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Business\Processor\Update;

use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateRequestTransfer;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateResponseTransfer;

interface ContentFieldUpdateProcessorInterface
{
    /**
     * @param ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
     * 
     * @return ContentFieldUpdateResponseTransfer 
     */
    public function updateContentField(
        ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
    ): ContentFieldUpdateResponseTransfer;
}
