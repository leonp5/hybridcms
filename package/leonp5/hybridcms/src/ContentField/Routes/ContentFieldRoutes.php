<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Routes;

use Illuminate\Support\Facades\Route;
use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\ContentField\Controller\ContentFieldController;

class ContentFieldRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::prefix('fields')->middleware('auth')->group(function () {
            Route::get('/{page}/{contentField}/{fieldId}/{langKey}', [ContentFieldController::class, 'edit'])
                ->name('field.edit');
            Route::post('/update', [ContentFieldController::class, 'update'])
                ->name('field.update');
        });
    }
}
