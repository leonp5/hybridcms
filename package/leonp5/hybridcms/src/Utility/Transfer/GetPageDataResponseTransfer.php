<?php

namespace Leonp5\Hybridcms\Utility\Transfer;

class GetPageDataResponseTransfer
{
    /**
     * @var bool
     */
    private bool $redirectNecessary = false;

    /**
     * @var PageDataTransfer
     */
    private PageDataTransfer $pageDataTransfer;

    /**
     * @var string
     */
    private string $viewString;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var int
     */
    private int $statusCode = 200;

    /**
     * @return  bool
     */
    public function redirectNecessary(): bool
    {
        return $this->redirectNecessary;
    }

    /**
     * @param  bool  $redirectNecessary
     *
     * @return  GetPageDataResponseTransfer
     */
    public function setRedirectNecessary(bool $redirectNecessary): GetPageDataResponseTransfer
    {
        $this->redirectNecessary = $redirectNecessary;

        return $this;
    }

    /**
     * @return string
     */
    public function getViewString(): string
    {
        return $this->viewString;
    }

    /**
     * @param string $viewString 
     *
     * @return GetPageDataResponseTransfer
     */
    public function setViewString(string $viewString): GetPageDataResponseTransfer
    {
        $this->viewString = $viewString;

        return $this;
    }

    /**
     * @return PageDataTransfer
     */
    public function getPageDataTransfer(): PageDataTransfer
    {
        return $this->pageDataTransfer;
    }

    /**
     * @param PageDataTransfer $pageDataTransfer
     *
     * @return GetPageDataResponseTransfer
     */
    public function setPageDataTransfer(PageDataTransfer $pageDataTransfer): GetPageDataResponseTransfer
    {
        $this->pageDataTransfer = $pageDataTransfer;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url 
     *
     * @return GetPageDataResponseTransfer
     */
    public function setUrl(string $url): GetPageDataResponseTransfer
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode 
     *
     * @return GetPageDataResponseTransfer
     */
    public function setStatusCode(int $statusCode): GetPageDataResponseTransfer
    {
        $this->statusCode = $statusCode;

        return $this;
    }
}
