<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility\Traits;

trait BoolIntCasterTrait
{

    /**
     * @param int $value
     * 
     * @return bool
     */
    public function toBool(int $value): bool
    {
        return boolval($value);
    }

    /**
     * @param bool $value
     * 
     * @return int
     */
    public function toInt(bool $value): int
    {
        return (int) $value;
    }
}
