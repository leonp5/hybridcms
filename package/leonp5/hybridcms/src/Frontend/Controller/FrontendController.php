<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Controller;

use Illuminate\Http\Request;
use InvalidArgumentException;

use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\Utility\Business\UtilityFacadeInterface;
use Leonp5\Hybridcms\Page\Business\Status\PageStatusInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Frontend\Business\FrontendFacadeInterface;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataRequestTransfer;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleRequestTransfer;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataResponseTransfer;
use Leonp5\Hybridcms\Frontend\Business\Processor\Localization\LocalizationProcessorInterface;

class FrontendController extends Controller
{
    /**
     * @param Request $request
     * @param UtilityFacadeInterface $utilityFacade
     * @param LocalizationProcessorInterface $localizationProcessor
     * @param FrontendFacadeInterface $frontendFacade
     * @param LanguageFacadeInterface $languageFacade
     * 
     * @return mixed 
     * @throws InvalidArgumentException 
     */
    public function handleRequest(
        Request $request,
        UtilityFacadeInterface $utilityFacade,
        LocalizationProcessorInterface $localizationProcessor,
        FrontendFacadeInterface $frontendFacade,
        LanguageFacadeInterface $languageFacade,
    ) {
        $getPageDataRequestTransfer = new GetPageDataRequestTransfer();
        $getPageDataRequestTransfer->setPageStatus(PageStatusInterface::PAGE_STATUS_PUBLISHED);

        if (env('FRONTEND_MULTILANG') === true) {
            $localizationProcessorResponseTransfer = $localizationProcessor
                ->hasLocale($request->url);
            if ($localizationProcessorResponseTransfer->hasLocale() === true) {
                $localizationProcessorResponseTransfer = $localizationProcessor->validate(
                    $localizationProcessorResponseTransfer
                );
                $getPageDataRequestTransfer->setLocale(
                    $localizationProcessorResponseTransfer->getLocale()
                );
                $getPageDataRequestTransfer->setUrl(
                    $localizationProcessorResponseTransfer->getUrl()
                );
            } else {
                $getPageDataRequestTransfer->setUrl('/' . $request->url);
            }

            $pageDataGetterResponseTransfer = $utilityFacade->findMultiLangPageData($getPageDataRequestTransfer);
        }

        if (env('FRONTEND_MULTILANG') === false) {
            if ($request->url === null) {
                $request->url = '';
            }
            $getPageDataRequestTransfer->setUrl('/' . $request->url);
            $getPageDataRequestTransfer->setLocale(
                $languageFacade->getMainLanguage()->alpha_2
            );

            $pageDataGetterResponseTransfer = $utilityFacade->findPageData($getPageDataRequestTransfer);
        }

        $pageAssemblerResponseTransfer = $frontendFacade->assemblePage(
            $this->createPageAssembleRequestTransfer($pageDataGetterResponseTransfer)
        );

        return $pageAssemblerResponseTransfer->getPage();
    }


    /**
     * @param GetPageDataResponseTransfer $pageDataGetterResponseTransfer
     * 
     * @return PageAssembleRequestTransfer
     */
    private function createPageAssembleRequestTransfer(
        GetPageDataResponseTransfer $pageDataGetterResponseTransfer
    ): PageAssembleRequestTransfer {

        $pageAssembleRequestTransfer = new PageAssembleRequestTransfer();

        $pageAssembleRequestTransfer->setUrl($pageDataGetterResponseTransfer->getUrl());
        $pageAssembleRequestTransfer->getStatusCode($pageDataGetterResponseTransfer->getStatusCode());
        $pageAssembleRequestTransfer->setRedirectNecessary($pageDataGetterResponseTransfer->redirectNecessary());

        if ($pageDataGetterResponseTransfer->redirectNecessary() === true) {
            return $pageAssembleRequestTransfer;
        }

        $pageAssembleRequestTransfer->setViewString($pageDataGetterResponseTransfer->getViewString());
        $pageAssembleRequestTransfer->setPageDataTransfer($pageDataGetterResponseTransfer->getPageDataTransfer());

        return $pageAssembleRequestTransfer;
    }
}
