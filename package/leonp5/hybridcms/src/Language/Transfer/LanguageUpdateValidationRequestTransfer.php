<?php

namespace Leonp5\Hybridcms\Language\Transfer;

class LanguageUpdateValidationRequestTransfer
{
    /**
     * @var null|array
     */
    private ?array $languages;

    /**
     * @param null|array $languages 
     */
    public function __construct(?array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return  null|array
     */
    public function getLanguages(): ?array
    {
        return $this->languages;
    }

    /**
     * Set the value of languages
     *
     * @param  null|array  $languages
     *
     * @return  LanguageUpdateValidationRequestTransfer
     */
    public function setLanguages($languages): LanguageUpdateValidationRequestTransfer
    {
        $this->languages = $languages;

        return $this;
    }
}
