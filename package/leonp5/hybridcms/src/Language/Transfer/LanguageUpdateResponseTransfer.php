<?php

namespace Leonp5\Hybridcms\Language\Transfer;

class LanguageUpdateResponseTransfer
{
    /**
     * @var bool
     */
    private bool $successful = false;

    /**
     * @return  bool
     */
    public function hasSuccess()
    {
        return $this->successful;
    }

    /**
     * @param  bool  $successful
     *
     * @return  LanguageUpdateResponseTransfer
     */
    public function setSuccessful(bool $successful): LanguageUpdateResponseTransfer
    {
        $this->successful = $successful;

        return $this;
    }
}
