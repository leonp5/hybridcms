<?php

namespace Leonp5\Hybridcms\Language\Transfer;

class FindLanguageResponseTransfer
{
    /**
     * @var string
     */
    private string $langName;

    /**
     * @var int
     */
    private int $langId;

    /**
     * @var string
     */
    private string $alpha_2;

    /**
     * @var string
     */
    private string $alpha_3;

    /**
     * @return string
     */
    public function getLangName(): string
    {
        return $this->langName;
    }

    /**
     * @param string $langName 
     *
     * @return self
     */
    public function setLangName(string $langName): self
    {
        $this->langName = $langName;

        return $this;
    }

    /**
     * @return int
     */
    public function getLangId(): int
    {
        return $this->langId;
    }

    /**
     * @param int $langId 
     *
     * @return self
     */
    public function setLangId(int $langId): self
    {
        $this->langId = $langId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlpha_2(): string
    {
        return $this->alpha_2;
    }

    /**
     * @param string $alpha_2 
     *
     * @return self
     */
    public function setAlpha_2(string $alpha_2): self
    {
        $this->alpha_2 = $alpha_2;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlpha_3(): string
    {
        return $this->alpha_3;
    }

    /**
     * @param string $alpha_3 
     *
     * @return self
     */
    public function setAlpha_3(string $alpha_3): self
    {
        $this->alpha_3 = $alpha_3;

        return $this;
    }
}
