<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\Container\BindingResolutionException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Leonp5\Hybridcms\App\Models\Languages;
use Leonp5\Hybridcms\App\Models\ActivatedLanguages;
use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateValidationRequestTransfer;
use Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdate\LanguageUpdateProcessorInterface;
use Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdateValidation\LanguageUpdateValidationProcessorInterface;

class AdminLanguagesController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (env('FRONTEND_MULTILANG') === true) {
            $activeLanguages = ActivatedLanguages::withDisabled();
            $allLanguages = Languages::findAll();

            foreach ($activeLanguages as $activeLang) {
                $lang = $allLanguages->where('id', $activeLang->lang_id)->first();
                $lang->enabled = true;
            }

            return view(
                'hybridcms::backend.admin.languages.index',
                [
                    'activeLanguages' => $activeLanguages,
                    'allLanguages' => $allLanguages
                ]
            );
        }

        $activeLanguages = [];
        return view('hybridcms::backend.admin.languages.index', ['activeLanguages' => $activeLanguages]);
    }

    /**
     * 
     * @param Request $request
     * 
     * @return RedirectResponse
     * @throws BindingResolutionException
     * @throws RouteNotFoundException
     */
    public function updateLanguages(
        Request $request,
        LanguageUpdateProcessorInterface $languageUpdateProcessor,
        LanguageUpdateValidationProcessorInterface $languageUpdateValidator
    ) {
        $languageUpdateValidationResponse = $languageUpdateValidator->validate(
            new LanguageUpdateValidationRequestTransfer($request->languages)
        );

        if ($languageUpdateValidationResponse->hasSuccess() === false) {
            return redirect()->back()->with('error', $languageUpdateValidationResponse->getMessage());
        }

        $languageUpdateResponse = $languageUpdateProcessor->update(
            new LanguageUpdateRequestTransfer($request->languages)
        );

        if ($languageUpdateResponse->hasSuccess() === false) {
            return redirect()->back()->with('error', __('hybridcms::admin.languages.update.error'));
        }

        return  redirect()->route('languages.index')->with('success', __('hybridcms::admin.languages.update.success'));
    }
}
