<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\ActivatedLanguage;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

final class ActivatedLanguageProcessor implements ActivatedLanguageProcessorInterface
{
    /**
     * @return mixed[]
     */
    public function get(): array
    {
        // getting the language name in the correct language
        $langTable = 'language_' . App::getLocale();

        $activeLanguages = [];

        $activatedLanguages = DB::table('activated_languages')
            ->select(['activated_languages.lang_id', 'activated_languages.main_language', 'activated_languages.disabled'])
            ->where('activated_languages.disabled', '=', false)
            ->get();

        foreach ($activatedLanguages as $language) {

            $activeLanguage = DB::table($langTable)
                ->select([$langTable . '.name as langName', $langTable . '.id', $langTable . '.alpha_2'])
                ->where($langTable . '.id', '=', $language->lang_id)
                ->first();

            $activeLanguages[] = (object) array_merge((array)$activeLanguage, (array)$language);
        }

        return $activeLanguages;
    }
}
