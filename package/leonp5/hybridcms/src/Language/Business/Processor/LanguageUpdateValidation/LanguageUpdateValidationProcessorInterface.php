<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdateValidation;

use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateValidationRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateValidationResponseTransfer;

interface LanguageUpdateValidationProcessorInterface
{

    /**
     * @param LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer 
     * 
     * @return LanguageUpdateValidationResponseTransfer 
     */
    public function validate(
        LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer
    ): LanguageUpdateValidationResponseTransfer;
}
