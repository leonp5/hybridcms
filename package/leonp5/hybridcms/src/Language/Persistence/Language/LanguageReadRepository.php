<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Persistence\Language;

use Illuminate\Support\Facades\DB;

use Leonp5\Hybridcms\Language\Transfer\FindLanguageRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\FindLanguageResponseTransfer;

final class LanguageReadRepository implements LanguageReadRepositoryInterface
{
    public function findLanguage(
        FindLanguageRequestTransfer $findLanguageRequestTransfer
    ): FindLanguageResponseTransfer {

        $langTable = 'language_' . $findLanguageRequestTransfer->getLangKey();

        $language = DB::table($langTable)
            ->select([$langTable . '.name as langName', $langTable . '.id', $langTable . '.alpha_2', $langTable . '.alpha_3'])
            ->where($langTable . '.alpha_2', '=', $findLanguageRequestTransfer->getWantedLangAlpha_2())
            ->first();

        return (new FindLanguageResponseTransfer())
            ->setLangName($language->langName)
            ->setLangId($language->id)
            ->setAlpha_2($language->alpha_2)
            ->setAlpha_3($language->alpha_3);
    }
}
