<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\Language\Business\LanguageFacade;
use Leonp5\Hybridcms\Content\Business\ContentFacadeInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeBusinessFactory;
use Leonp5\Hybridcms\ContentField\Business\ContentFieldFacadeInterface;
use Leonp5\Hybridcms\Language\Persistence\Language\LanguageReadRepository;
use Leonp5\Hybridcms\Language\Persistence\Language\LanguageReadRepositoryInterface;
use Leonp5\Hybridcms\Language\Business\Processor\MainLanguage\MainLanguageProcessor;
use Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdate\LanguageUpdateProcessor;
use Leonp5\Hybridcms\Language\Business\Processor\MainLanguage\MainLanguageProcessorInterface;
use Leonp5\Hybridcms\Language\Business\Processor\ActivatedLanguage\ActivatedLanguageProcessor;
use Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdate\LanguageUpdateProcessorInterface;
use Leonp5\Hybridcms\Language\Business\Processor\ActivatedLanguage\ActivatedLanguageProcessorInterface;
use Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdateValidation\LanguageUpdateValidationProcessor;
use Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdateValidation\LanguageUpdateValidationProcessorInterface;

final class LanguageDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(LanguageFacadeBusinessFactory::class);

        $app->bind(LanguageFacadeInterface::class, function ($app) {
            return new LanguageFacade(
                $app->make(LanguageFacadeBusinessFactory::class)
            );
        });

        $app->bind(
            LanguageUpdateProcessorInterface::class,
            function ($app) {
                return new LanguageUpdateProcessor(
                    $app->make(ContentFacadeInterface::class),
                    $app->make(ContentFieldFacadeInterface::class),
                );
            }
        );
        $app->bind(
            LanguageUpdateValidationProcessorInterface::class,
            LanguageUpdateValidationProcessor::class
        );

        $app->bind(
            MainLanguageProcessorInterface::class,
            MainLanguageProcessor::class
        );

        $app->bind(
            ActivatedLanguageProcessorInterface::class,
            ActivatedLanguageProcessor::class
        );

        $app->bind(
            LanguageReadRepositoryInterface::class,
            LanguageReadRepository::class
        );
    }
}
