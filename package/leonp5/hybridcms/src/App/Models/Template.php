<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Template extends Model
{
    use HasFactory;

    protected $table = 'template';

    protected $fillable = ['template_name', 'component', 'single_use', 'partial', 'disabled', 'view_string'];

    /**
     * @var string[]
     */
    protected $casts = [
        'component' => 'boolean',
        'single_use' => 'boolean',
        'partial' => 'boolean',
        'disabled' => 'boolean',
    ];

    /**
     * @return HasMany 
     */
    public function templateField(): HasMany
    {
        return $this->hasMany(TemplateField::class);
    }
}
