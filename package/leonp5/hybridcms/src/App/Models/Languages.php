<?php

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Languages extends Model
{
    use HasFactory;

    /**
     * @return Collection 
     */
    public static function findAll()
    {
        $langTable = 'language_' . App::getLocale();

        return DB::table($langTable)
            ->get();
    }
}
