<?php

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TemplateScanOption extends Model
{
    use HasFactory;

    protected $fillable = ['check_unused', 'scan_partials', 'scan_components'];

    /**
     * @var string[]
     */
    protected $casts = [
        'scan_partials' => 'boolean',
        'scan_components' => 'boolean',
        'check_unused' => 'boolean',
    ];
}
