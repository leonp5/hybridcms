<?php

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PageStatus extends Model
{
    use HasFactory;

    /**
     * 
     * @return BelongsToMany 
     */
    public function pages(): BelongsToMany
    {
        return $this->belongsToMany('Leonp5\Hybridcms\App\Models\Page');
    }
}
