<?php

namespace Leonp5\Hybridcms\Page\Transfer;

use Illuminate\Support\Collection;

class TemplateDataTransfer
{
    /**
     * @var object
     */
    private object $template;

    /**
     * @var Collection
     */
    private Collection $templateFields;

    /**
     * @var Collection
     */
    private Collection $pageStatusLabels;

    /**
     * @var array
     */
    private array $activeLanguages;

    /**
     * @return object
     */
    public function getTemplate(): object
    {
        return $this->template;
    }

    /**
     * @param object $template 
     *
     * @return TemplateDataTransfer
     */
    public function setTemplate(object $template): TemplateDataTransfer
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTemplateFields(): Collection
    {
        return $this->templateFields;
    }

    /**
     * @param Collection $templateFields 
     *
     * @return TemplateDataTransfer
     */
    public function setTemplateFields(Collection $templateFields): TemplateDataTransfer
    {
        $this->templateFields = $templateFields;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPageStatusLabels(): Collection
    {
        return $this->pageStatusLabels;
    }

    /**
     * @param Collection $pageStatusLabels 
     *
     * @return TemplateDataTransfer
     */
    public function setPageStatusLabels(Collection $pageStatusLabels): TemplateDataTransfer
    {
        $this->pageStatusLabels = $pageStatusLabels;

        return $this;
    }

    /**
     * @return array
     */
    public function getActiveLanguages(): array
    {
        return $this->activeLanguages;
    }

    /**
     * @param array $activeLanguages 
     *
     * @return TemplateDataTransfer
     */
    public function setActiveLanguages(array $activeLanguages): TemplateDataTransfer
    {
        $this->activeLanguages = $activeLanguages;

        return $this;
    }
}
