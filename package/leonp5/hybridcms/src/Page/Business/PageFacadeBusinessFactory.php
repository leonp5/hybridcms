<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page\Business;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Page\Business\Processor\Create\PageCreateProcessorInterface;

final class PageFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return PageCreateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getPageCreateProcesor(): PageCreateProcessorInterface
    {
        return $this->app->make(PageCreateProcessorInterface::class);
    }
}
