<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page\Business\Processor\Create;

use InvalidArgumentException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

use Leonp5\Hybridcms\Page\Transfer\TemplateDataTransfer;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;

use stdClass;

final class PageCreateProcessor implements PageCreateProcessorInterface
{

    /**
     * @var LanguageFacadeInterface
     */
    private LanguageFacadeInterface $languageFacade;

    public function __construct(LanguageFacadeInterface $languageFacade)
    {
        $this->languageFacade = $languageFacade;
    }

    /**
     * @param int $templateId
     * 
     * @return object 
     * @throws InvalidArgumentException 
     */
    public function createNewPage(int $templateId): object
    {
        $templateDataTransfer  = $this->createTemplateData($templateId);

        $pageToEdit = $this->createPageDummyWithAllLang($templateDataTransfer);

        $page = $pageToEdit;
        $page->activeLanguages = $templateDataTransfer->getActiveLanguages();
        $page->template = $templateDataTransfer->getTemplate();
        $page->templateFields = $templateDataTransfer->getTemplateFields();
        $page->statusLabels = $templateDataTransfer->getPageStatusLabels();

        return $page;
    }

    /**
     * @param int $pageId
     * 
     * @return object 
     * @throws InvalidArgumentException 
     */
    public function createExistingPage(int $pageId): object
    {
        $pageToEdit = DB::table('pages')
            ->where('pages.id', '=', $pageId)
            ->select([
                'pages.id', 'pages.user_id', 'pages.available_lang', 'pages.page_status_id', 'template_id'
            ])
            ->first();

        $templateDataTransfer = $this->createTemplateData($pageToEdit->template_id);

        $pageToEdit = $this->createPageWithAllLang($pageToEdit, $templateDataTransfer->getActiveLanguages());

        $page = $pageToEdit;
        $page->activeLanguages = $templateDataTransfer->getActiveLanguages();
        $page->template = $templateDataTransfer->getTemplate();
        $page->templateFields = $templateDataTransfer->getTemplateFields();
        $page->statusLabels = $templateDataTransfer->getPageStatusLabels();

        return $page;
    }

    /**
     * @param TemplateDataTransfer $templateDataTransfer
     * 
     * @return object 
     */
    private function createPageDummyWithAllLang(TemplateDataTransfer $templateDataTransfer): object
    {
        $page = new stdClass();

        foreach ($templateDataTransfer->getActiveLanguages() as $activeLanguage) {

            $langKey = $activeLanguage->alpha_2;

            $generalContent =  new stdClass();
            $generalContent->title = null;
            $generalContent->url = null;
            $generalContent->description = null;

            $page->general_content[$langKey] = $generalContent;

            $pageContent = [];

            foreach ($templateDataTransfer->getTemplateFields() as $contentField) {
                $pageContent[$contentField->field_name] = [
                    'content_type_id' => $contentField->content_type_id,
                    'content' => null,
                    'field_id' => null,
                    'template_field_id' => $contentField->id
                ];
            }

            $page->page_content[$langKey] = $pageContent;
        }

        return $page;
    }

    /**
     * @param object $page
     * @param array $activeLanguages
     * 
     * @return array 
     * @throws InvalidArgumentException 
     */
    private function createPageWithAllLang(object $page, array $activeLanguages): object
    {
        foreach ($activeLanguages as $activeLanguage) {

            $langKey = $activeLanguage->alpha_2;

            $contentTable = ContentTableInterface::CONTENT_TABLE_PREFIX . $langKey;

            $generalContent = DB::table('pages')
                ->join($contentTable, 'pages.id', '=', $contentTable . '.page_id')
                ->where($contentTable . '.page_id', '=', $page->id)
                ->select([
                    $contentTable . '.title', $contentTable . '.url',
                    $contentTable . '.description'
                ])
                ->first();

            $page->general_content[$langKey] = $generalContent;

            $contentFieldTable = ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey;

            $pageContentCollection = DB::table($contentFieldTable)
                ->join('template_field', 'template_field.id', '=', $contentFieldTable . '.template_field_id')
                ->where($contentFieldTable . '.page_id', '=', $page->id)
                ->select([
                    'template_field.field_name',
                    'template_field.id as template_field_id',
                    $contentFieldTable . '.id',
                    $contentFieldTable . '.content',
                    $contentFieldTable . '.content_type_id'
                ])
                ->get()->toArray();

            $pageContent = [];

            foreach ($pageContentCollection as $key => $contentField) {
                $pageContent[$contentField->field_name] = [
                    'content_type_id' => $contentField->content_type_id,
                    'content' => $contentField->content,
                    'field_id' => $contentField->id,
                    'template_field_id' => $contentField->template_field_id
                ];
            }

            $page->page_content[$langKey] = $pageContent;
        }

        return $page;
    }

    /**
     * @param int $templateId
     * 
     * @return TemplateDataTransfer 
     * @throws InvalidArgumentException 
     */
    private function createTemplateData(int $templateId): TemplateDataTransfer
    {

        $templateDataTransfer = new TemplateDataTransfer();

        $pageStatusLangTable = $this->createPageStatusLangTable();

        $templateDataTransfer->setPageStatusLabels($this->getStatusLabels($pageStatusLangTable));

        $templateDataTransfer->setTemplateFields($this->getTemplateFields($templateId));

        $templateDataTransfer->setTemplate($this->getTemplate($templateId));

        $templateDataTransfer->setActiveLanguages($this->languageFacade->getActivatedLanguages());

        return $templateDataTransfer;
    }

    /**
     * @return string 
     */
    private function createPageStatusLangTable(): string
    {
        $lang = App::getLocale();

        return 'page_statuses_' . $lang;
    }

    /**
     * @param int $templateId
     * 
     * @return object 
     * @throws InvalidArgumentException 
     */
    private function getTemplate(int $templateId): object
    {
        return DB::table('template')
            ->select(['template_name', 'id'])
            ->where('template.id', '=', $templateId)
            ->first();
    }

    /**
     * @param int $templateId
     * 
     * @return Collection 
     * @throws InvalidArgumentException 
     */
    private function getTemplateFields(int $templateId): Collection
    {
        return DB::table('template_field')
            ->select(['field_name', 'content_type_id', 'template_field.id', 'template_id'])
            ->where('template_field.template_id', '=', $templateId)
            ->get();
    }

    /**
     * @param string $pageStatusLangTable
     * 
     * @return Collection 
     * @throws InvalidArgumentException 
     */
    private function getStatusLabels(string $pageStatusLangTable): Collection
    {
        return DB::table($pageStatusLangTable)
            ->select([$pageStatusLangTable . '.id', $pageStatusLangTable . '.status_label'])
            ->get();
    }
}
