<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Controller;

use Illuminate\Http\JsonResponse;

use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\Template\Business\TemplateFacadeInterface;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateRequestTransfer;

class TemplateController extends Controller
{
    /**
     * @return JsonResponse 
     * 
     * @throws InvalidArgumentException 
     * @throws BindingResolutionException 
     */
    public function searchTemplates(TemplateFacadeInterface $templateFacade): JsonResponse
    {
        $templates = $templateFacade->searchTemplates(
            (new SearchTemplateRequestTransfer)
                ->setAlreadyUsedSingleUseTemplatesIncluded(false)
        );

        return response()->json(['templates' => $templates->getTemplateCollection()]);
    }
}
