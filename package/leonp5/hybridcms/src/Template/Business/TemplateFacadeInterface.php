<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business;

use Illuminate\Database\Eloquent\Collection;

use Leonp5\Hybridcms\Template\Transfer\TemplateTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateScanResponseTransfer;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateRequestTransfer;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateResponseTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateUpdateResponseTransfer;

interface TemplateFacadeInterface
{
    /**
     * @return TemplateScanResponseTransfer 
     */
    public function scanPageTemplates(): TemplateScanResponseTransfer;

    /**
     * @return TemplateScanResponseTransfer 
     */
    public function scanPartials(): TemplateScanResponseTransfer;

    /**
     * @param TemplateTransfer $templateTransfer
     * 
     * @return TemplateUpdateResponseTransfer
     */
    public function updateTemplateTables(
        TemplateTransfer $templateTransfer
    ): TemplateUpdateResponseTransfer;

    /**
     * @param SearchTemplateRequestTransfer $searchTemplateRequestTransfer
     * 
     * @return SearchTemplateResponseTransfer
     */
    public function searchTemplates(
        SearchTemplateRequestTransfer $searchTemplateRequestTransfer
    ): SearchTemplateResponseTransfer;

    /**
     * @return null|Collection 
     */
    public function searchUnusedTemplates(): ?Collection;
}
