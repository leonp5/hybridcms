<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\UnusedTemplates;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

use Leonp5\Hybridcms\App\Models\Template;

final class SearchUnusedTemplatesProcessor implements SearchUnusedTemplatesProcessorInterface
{
    /**
     * @return null|Collection
     */
    public function search(): ?Collection
    {
        $templates = Template::where([
            ['disabled', '=',  false],
            ['component', '=', false],
            ['partial', '=', false],
        ])->get(['id', 'template_name', 'single_use']);


        $pageTemplateIds = DB::table('pages')
            ->get('template_id')->toArray();

        $pageTemplateIds = array_map(function ($pageTemplateIdObject) {
            return $pageTemplateIdObject->template_id;
        }, $pageTemplateIds);

        foreach ($templates as $key => $pageTemplate) {
            $templateUsed = array_search($pageTemplate->id, $pageTemplateIds);

            if ($templateUsed !== false) {
                $templates = $templates->filter(function ($template) use ($pageTemplate) {
                    return $template->id != $pageTemplate->id;
                })->values();
            }
        }

        if ($templates->count() > 0) {
            return $templates;
        }

        return null;
    }
}
