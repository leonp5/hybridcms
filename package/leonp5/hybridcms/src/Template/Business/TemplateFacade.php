<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Container\BindingResolutionException;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateRequestTransfer;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateResponseTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateScanResponseTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateUpdateResponseTransfer;

final class TemplateFacade implements TemplateFacadeInterface
{
    /**
     * @var TemplateFacadeBusinessFactory
     */
    private TemplateFacadeBusinessFactory $factory;

    /**
     * @param TemplateFacadeBusinessFactory $factory
     * 
     * @return void
     */
    public function __construct(TemplateFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return TemplateScanResponseTransfer
     * @throws BindingResolutionException
     */
    public function scanPageTemplates(): TemplateScanResponseTransfer
    {
        return $this->factory
            ->getScanTemplateProcessor()
            ->scanPageTemplates();
    }

    /**
     * @return TemplateScanResponseTransfer
     * @throws BindingResolutionException
     */
    public function scanPartials(): TemplateScanResponseTransfer
    {
        return $this->factory
            ->getScanTemplateProcessor()
            ->scanPartials();
    }

    /**
     * @param TemplateTransfer $templateTransfer
     * 
     * @return TemplateUpdateResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function updateTemplateTables(
        TemplateTransfer $templateTransfer
    ): TemplateUpdateResponseTransfer {

        return $this->factory
            ->getUpdateTemplateTableProcessor()
            ->update($templateTransfer);
    }

    /**
     * @param SearchTemplateRequestTransfer $searchTemplateRequestTransfer
     * 
     * @return SearchTemplateResponseTransfer
     * @throws BindingResolutionException
     */
    public function searchTemplates(
        SearchTemplateRequestTransfer $searchTemplateRequestTransfer
    ): SearchTemplateResponseTransfer {

        return $this->factory
            ->getSearchTemplateProcessor()
            ->search($searchTemplateRequestTransfer);
    }

    /**
     * @return null|Collection
     * @throws BindingResolutionException
     */
    public function searchUnusedTemplates(): ?Collection
    {
        return $this->factory
            ->getSearchUnusedTemplatesProcessor()
            ->search();
    }
}
