<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Template;

interface TemplateInterface
{
    public const VIEW_PREFIX = 'hybridcms::frontend.';
    public const COMPONENT_TAG = 'data-hcms-component';
    public const PARTIAL_TAG = 'data-hcms-partial';
    public const SINGLE_USE_TAG = 'data-hcms-single-use';
    public const INCLUDES_SINGLE_USE_TAGS = 'data-hcms-includes-single-uses';
}
