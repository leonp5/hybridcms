<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class TemplateFileTransfer
{

    /**
     * @var string
     */
    private string $filePath;

    /**
     * @var string
     */
    private string $templateName;

    /**
     * @var string
     */
    private string $viewString;

    /**
     * @var bool
     */
    private bool $singleUse = false;

    /**
     * @var bool
     */
    private bool $component = false;

    /**
     * @var bool
     */
    private bool $partial = false;

    /**
     * @var ContentFieldTransfer[]
     */
    private array $contentFieldTransfers = [];

    /**
     * @return  array
     */
    public function getContentFieldTransfers(): array
    {
        return $this->contentFieldTransfers;
    }

    /**
     * @param  array $contentFieldTransfers
     *
     * @return  TemplateFileTransfer
     */
    public function addContentFieldTransfer(ContentFieldTransfer $contentFieldTransfer): TemplateFileTransfer
    {
        $this->contentFieldTransfers[] = $contentFieldTransfer;

        return $this;
    }

    /**
     * @return  string
     */
    public function getTemplateName(): string
    {
        return $this->templateName;
    }

    /**
     * @param  string  $templateName
     *
     * @return  TemplateFileTransfer
     */
    public function setTemplateName(string $templateName): TemplateFileTransfer
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSingleUse(): bool
    {
        return $this->singleUse;
    }

    /**
     * @param  bool $singleUse
     *
     * @return TemplateFileTransfer
     */
    public function setSingleUse(bool $singleUse): TemplateFileTransfer
    {
        $this->singleUse = $singleUse;

        return $this;
    }

    /**
     * @return bool
     */
    public function getComponent(): bool
    {
        return $this->component;
    }

    /**
     * @param bool $component
     *
     * @return TemplateFileTransfer
     */
    public function setComponent(bool $component): TemplateFileTransfer
    {
        $this->component = $component;

        return $this;
    }

    /**
     * @return bool
     */
    public function getPartial(): bool
    {
        return $this->partial;
    }

    /**
     * @param bool $partial
     *
     * @return TemplateFileTransfer
     */
    public function setPartial(bool $partial): TemplateFileTransfer
    {
        $this->partial = $partial;

        return $this;
    }

    /**
     * @return string
     */
    public function getViewString(): string
    {
        return $this->viewString;
    }

    /**
     * @param string $viewString
     *
     * @return TemplateFileTransfer
     */
    public function setViewString(string $viewString): TemplateFileTransfer
    {
        $this->viewString = $viewString;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath 
     *
     * @return self
     */
    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }
}
