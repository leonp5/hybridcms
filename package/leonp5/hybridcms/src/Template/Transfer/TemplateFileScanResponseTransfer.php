<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class TemplateFileScanResponseTransfer
{
    /**
     * @var bool
     */
    private bool $successful = true;

    /**
     * @var string
     */
    private string $message;

    /**
     * @var TemplateFileTransfer
     */
    private TemplateFileTransfer $templateFileTransfer;


    /**
     * @return  bool
     */
    public function hasSuccess()
    {
        return $this->successful;
    }

    /**
     * @param  bool  $successful
     *
     * @return  TemplateFileScanResponseTransfer
     */
    public function setSuccessful(bool $successful): TemplateFileScanResponseTransfer
    {
        $this->successful = $successful;

        return $this;
    }

    /**
     * @return TemplateFileTransfer
     */
    public function getTemplateFileTransfer(): TemplateFileTransfer
    {
        return $this->templateFileTransfer;
    }

    /**
     * @param TemplateFileTransfer $templateFileTransfer
     *
     * @return TemplateFileScanResponseTransfer
     */
    public function setTemplateFileTransfer(
        TemplateFileTransfer $templateFileTransfer
    ): TemplateFileScanResponseTransfer {
        $this->templateFileTransfer = $templateFileTransfer;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param  string  $message
     *
     * @return  TemplateFileScanResponseTransfer
     */
    public function setMessage(string $message): TemplateFileScanResponseTransfer
    {
        $this->message = $message;

        return $this;
    }
}
