<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Update;

use Carbon\Carbon;
use RuntimeException;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Leonp5\Hybridcms\Content\Transfer\ContentUpdateRequestTransfer;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Content\Transfer\ContentUpdateResponseTransfer;

final class ContentUpdateProcessor implements ContentUpdateProcessorInterface
{
    /**
     * @param ContentUpdateRequestTransfer $contentUpdateRequestTransfer
     * 
     * @return ContentUpdateResponseTransfer
     * @throws InvalidArgumentException
     */
    public function updateContent(
        ContentUpdateRequestTransfer $contentUpdateRequestTransfer
    ): ContentUpdateResponseTransfer {
        $contentUpdateResponseTransfer = new ContentUpdateResponseTransfer();

        $availableLanguages = [];

        $currentUserId = Auth::user()->id;
        $pageStatusId = $contentUpdateRequestTransfer->getPageStatusId();
        $pageId = $contentUpdateRequestTransfer->getPageId();

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'user_id' => $currentUserId,
                'page_status_id' => $pageStatusId,
                'updated_at' => Carbon::now()
            ]);

        // save the inputs for each language
        foreach ($contentUpdateRequestTransfer->getGeneralContent() as $langKey => $content) {
            $title = $content['title'];
            $url = $content['url'];
            $description = $content['description'];

            DB::table(ContentTableInterface::CONTENT_TABLE_PREFIX . $langKey)
                ->updateOrInsert(
                    ['page_id' => $pageId],
                    [
                        'title' => $title,
                        'url' => $url,
                        'description' => $description
                    ]
                );

            $availableLanguages[] = $langKey;
        }

        $this->updatePageContent($contentUpdateRequestTransfer);

        $languagesString = implode(', ', $availableLanguages);

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'available_lang' => $languagesString,
                'page_status_id' => $pageStatusId
            ]);

        return $contentUpdateResponseTransfer;
    }

    /**
     * @param ContentUpdateRequestTransfer $contentUpdateRequestTransfer
     * 
     * @return void
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    private function updatePageContent(
        ContentUpdateRequestTransfer $contentUpdateRequestTransfer
    ) {

        foreach ($contentUpdateRequestTransfer->getPageContent() as $langKey => $pageContent) {
            foreach ($pageContent as $templateField) {

                if (array_key_exists('content', $templateField) === false) {
                    continue;
                }

                DB::table(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey)
                    ->updateOrInsert(
                        [
                            'page_id' => $contentUpdateRequestTransfer->getPageId(), 'id' => $templateField['field_id']
                        ],
                        [
                            'template_field_id' => $templateField['template_field_id'],
                            'content' => $templateField['content'],
                            'content_type_id' => $templateField['content_type_id'],
                            'created_at' => Carbon::now()
                        ]
                    );
            }
        }
    }
}
