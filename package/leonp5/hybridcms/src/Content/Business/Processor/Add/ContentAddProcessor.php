<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Add;

use Carbon\Carbon;
use RuntimeException;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

use Leonp5\Hybridcms\Page\Business\Status\PageStatusInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Content\Transfer\ContentAddRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentAddResponseTransfer;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateRequestTransfer;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateResponseTransfer;

class ContentAddProcessor implements ContentAddProcessorInterface
{
    /**
     * @param ContentAddRequestTransfer $contentAddRequestTransfer
     * 
     * @return ContentAddResponseTransfer
     * @throws InvalidArgumentException
     */
    public function add(
        ContentAddRequestTransfer $contentAddRequestTransfer,
    ): ContentAddResponseTransfer {
        $availableLanguages = [];
        $contentAddResponseTransfer = new ContentAddResponseTransfer();

        $currentUserId = Auth::user()->id;
        $pageStatusId = $contentAddRequestTransfer->getPageStatusId();
        $templateId = $contentAddRequestTransfer->getTemplateId();

        $pageId = DB::table('pages')->insertGetId(
            [
                'user_id' => $currentUserId,
                'page_status_id' => $pageStatusId,
                'template_id' => $templateId,
                'created_at' => Carbon::now()
            ]
        );

        // save the inputs for each language
        $atLeastOneUrl = false;

        foreach ($contentAddRequestTransfer->getGeneralContent() as $langKey => $content) {
            $title = $content['title'];
            $url = $content['url'];
            $description = $content['description'];

            if ($url !== null && $atLeastOneUrl === false) {
                $atLeastOneUrl = true;
            }

            DB::table(ContentTableInterface::CONTENT_TABLE_PREFIX . $langKey)->insert(
                [
                    'page_id' => $pageId,
                    'title' => $title,
                    'url' => $url,
                    'description' => $description
                ]
            );

            $availableLanguages[] = $langKey;
        }

        $this->insertPageContent($contentAddRequestTransfer, $pageId);

        $languagesString = implode(', ', $availableLanguages);

        // if no URL exists, make the page to a draft
        if ($atLeastOneUrl === false) {
            $pageStatusId = PageStatusInterface::PAGE_STATUS_DRAFT;
        }

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'available_lang' => $languagesString,
                'page_status_id' => $pageStatusId
            ]);

        return $contentAddResponseTransfer;
    }

    /**
     * @param Request $request
     * @param array $contentLanguageCodes
     * @param int $pageId
     * 
     * @return void 
     * @throws InvalidArgumentException 
     */
    public function updateContent(
        Request $request,
        int $pageId
    ) {
        $availableLanguages = [];

        $currentUserId = Auth::user()->id;
        $pageStatusId = $request->input('page_status');

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'user_id' => $currentUserId,
                'page_status_id' => $pageStatusId,
                'updated_at' => Carbon::now()
            ]);

        // save the inputs for each language
        foreach ($request->general_content as $langKey => $content) {
            $title = $content['title'];
            $url = $content['url'];
            $description = $content['description'];

            DB::table(ContentTableInterface::CONTENT_TABLE_PREFIX . $langKey)
                ->updateOrInsert(
                    ['page_id' => $pageId],
                    [
                        'title' => $title,
                        'url' => $url,
                        'description' => $description
                    ]
                );

            $availableLanguages[] = $langKey;
        }

        $this->updatePageContent($request, $pageId);

        $languagesString = implode(', ', $availableLanguages);

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'available_lang' => $languagesString,
                'page_status_id' => $pageStatusId
            ]);
    }

    /**
     * @param ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
     * 
     * @return ContentFieldUpdateResponseTransfer 
     */
    public function updateContentField(
        ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer,
    ): ContentFieldUpdateResponseTransfer {

        $contentFieldUpdateResponseTransfer = new ContentFieldUpdateResponseTransfer();

        $languageFacade = App::make(LanguageFacadeInterface::class);

        $langKey = $languageFacade->getMainLanguage()->alpha_2;

        $updateResponse = DB::table(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey)
            ->where(
                [
                    'page_id' => $contentFieldUpdateRequestTransfer->getPageId(),
                    'id' => $contentFieldUpdateRequestTransfer->getFieldId()
                ]
            )
            ->update(
                [
                    'content' => $contentFieldUpdateRequestTransfer->getContent(),
                    'updated_at' => Carbon::now()
                ]
            );

        if ($updateResponse !== 1) {
            $contentFieldUpdateResponseTransfer->setSuccess(false);
            $contentFieldUpdateResponseTransfer->setMessage(
                __('hybridcms::pages.content.field.update.error')
            );
            return $contentFieldUpdateResponseTransfer;
        }
        $contentFieldUpdateResponseTransfer->setMessage(
            __('hybridcms::pages.content.field.update.success', ['fieldName' =>
            $contentFieldUpdateRequestTransfer->getFieldName()])
        );

        return $contentFieldUpdateResponseTransfer;
    }

    /**
     * @param ContentAddRequestTransfer $contentAddRequestTransfer
     * @param int $pageId
     * 
     * @return void 
     */
    private function insertPageContent(
        ContentAddRequestTransfer $contentAddRequestTransfer,
        int $pageId
    ) {

        if ($contentAddRequestTransfer->getPageContent() !== null) {
            foreach ($contentAddRequestTransfer->getPageContent() as $langKey => $pageContent) {
                foreach ($pageContent as $templateField) {

                    if (array_key_exists('content', $templateField) === false) {
                        continue;
                    }

                    DB::table(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey)->insert(
                        [
                            'page_id' => $pageId,
                            'template_field_id' => $templateField['template_field_id'],
                            'content' => $templateField['content'],
                            'content_type_id' => $templateField['content_type_id'],
                            'created_at' => Carbon::now()
                        ]
                    );
                }
            }
        }
    }

    /**
     * @param Request $request
     * @param int $pageId
     * 
     * @return void 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     */
    private function updatePageContent(
        Request $request,
        int $pageId
    ) {

        foreach ($request->page_content as $langKey => $pageContent) {
            foreach ($pageContent as $templateField) {

                if (array_key_exists('content', $templateField) === false) {
                    continue;
                }

                DB::table(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey)
                    ->updateOrInsert(
                        [
                            'page_id' => $pageId, 'id' => $templateField['field_id']
                        ],
                        [
                            'template_field_id' => $templateField['template_field_id'],
                            'content' => $templateField['content'],
                            'content_type_id' => $templateField['content_type_id'],
                            'created_at' => Carbon::now()
                        ]
                    );
            }
        }
    }
}
