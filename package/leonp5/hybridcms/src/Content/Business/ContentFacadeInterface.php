<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business;

use Leonp5\Hybridcms\Content\Transfer\ContentAddRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentAddResponseTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentUpdateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentUpdateResponseTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateResponseTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataResponseTransfer;

interface ContentFacadeInterface
{
    /**
     * @param string $tableName
     * 
     * @return void
     */
    public function createTable(string $tableName): void;

    /**
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer
     */
    public function validateContentTitle(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer
    ): ContentValidateResponseTransfer;

    /**
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer
     */
    public function validateContentUrl(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer
    ): ContentValidateResponseTransfer;

    /**
     * @param ContentAddRequestTransfer $contentAddRequestTransfer
     * 
     * @return ContentAddResponseTransfer
     * @throws InvalidArgumentException
     */
    public function addContent(
        ContentAddRequestTransfer $contentAddRequestTransfer,
    ): ContentAddResponseTransfer;

    /**
     * @param ContentUpdateRequestTransfer $contentUpdateRequestTransfer
     * 
     * @return ContentUpdateResponseTransfer
     */
    public function updateContent(
        ContentUpdateRequestTransfer $contentUpdateRequestTransfer
    ): ContentUpdateResponseTransfer;

    /**
     * @param ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
     * 
     * @return ContentFieldDataResponseTransfer
     */
    public function getContentFieldData(
        ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
    ): ContentFieldDataResponseTransfer;
}
