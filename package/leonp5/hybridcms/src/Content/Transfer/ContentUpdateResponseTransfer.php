<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Transfer;

class ContentUpdateResponseTransfer
{
    /**
     * @var bool
     */
    private bool $success;

    /**
     * @var string
     */
    private string $message;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->success = true;
    }

    /**
     * @return bool
     */
    public function hasSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     *
     * @return self
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return self
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }
}
