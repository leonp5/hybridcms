<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Transfer;

class ContentValidateRequestTransfer
{
    /**
     * @var mixed[]
     */
    private array $generalContent;

    /**
     * @var null|int
     */
    private ?int $pageId = null;

    /**
     * @return mixed[]
     */
    public function getGeneralContent(): array
    {
        return $this->generalContent;
    }

    /**
     * @param mixed[] $generalContent 
     *
     * @return self
     */
    public function setGeneralContent(array $generalContent): self
    {
        $this->generalContent = $generalContent;

        return $this;
    }

    /**
     * @return null|int
     */
    public function getPageId(): null|int
    {
        return $this->pageId;
    }

    /**
     * @param null|int $pageId 
     *
     * @return self
     */
    public function setPageId(int $pageId): self
    {
        $this->pageId = $pageId;

        return $this;
    }
}
