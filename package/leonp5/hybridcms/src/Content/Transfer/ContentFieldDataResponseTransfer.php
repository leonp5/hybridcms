<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Transfer;

class ContentFieldDataResponseTransfer
{

    /**
     * @var bool
     */
    private bool $success = true;

    /**
     * @var string
     */
    private string $fieldName;

    /**
     * @var string
     */
    private string $contentData;

    /**
     * @var int
     */
    private int $fieldId;

    /**
     * @var int
     */
    private int $pageId;

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success 
     *
     * @return ContentFieldDataResponseTransfer
     */
    public function setSuccess(bool $success): ContentFieldDataResponseTransfer
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName 
     *
     * @return ContentFieldDataResponseTransfer
     */
    public function setFieldName(string $fieldName): ContentFieldDataResponseTransfer
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentData(): string
    {
        return $this->contentData;
    }

    /**
     * @param string $contentData 
     *
     * @return ContentFieldDataResponseTransfer
     */
    public function setContentData(string $contentData): ContentFieldDataResponseTransfer
    {
        $this->contentData = $contentData;

        return $this;
    }

    /**
     * @return int
     */
    public function getFieldId(): int
    {
        return $this->fieldId;
    }

    /**
     * @param int $fieldId 
     *
     * @return ContentFieldDataResponseTransfer
     */
    public function setFieldId(int $fieldId): ContentFieldDataResponseTransfer
    {
        $this->fieldId = $fieldId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageId(): int
    {
        return $this->pageId;
    }

    /**
     * @param int $pageId 
     *
     * @return self
     */
    public function setPageId(int $pageId): self
    {
        $this->pageId = $pageId;

        return $this;
    }
}
