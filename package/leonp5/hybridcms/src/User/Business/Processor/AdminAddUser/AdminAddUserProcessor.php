<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\AdminAddUser;

use Leonp5\Hybridcms\User\Transfer\UserAddRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserAddResponseTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Persistence\UserWriteRepositoryInterface;
use Leonp5\Hybridcms\User\Business\Processor\UserDataValidation\UserDataValidationProcessorInterface;

class AdminAddUserProcessor implements AdminAddUserProcessorInterface
{

    /**
     * @var UserDataValidationProcessorInterface
     */
    private UserDataValidationProcessorInterface $userDataValidator;

    /**
     * @var UserWriteRepositoryInterface
     */
    private UserWriteRepositoryInterface $userWriteRepository;

    public function __construct(
        UserDataValidationProcessorInterface $userDataValidator,
        UserWriteRepositoryInterface $userWriteRepository
    ) {
        $this->userDataValidator = $userDataValidator;
        $this->userWriteRepository = $userWriteRepository;
    }

    /**
     * @param UserAddRequestTransfer $userAddRequestTransfer
     * 
     * @return UserAddResponseTransfer 
     */
    public function add(UserAddRequestTransfer $userAddRequestTransfer): UserAddResponseTransfer
    {
        $userDataValidateRequestTransfer = $this->createUserDataValidateRequestTransfer($userAddRequestTransfer);

        $userDataValidationResponse = $this->userDataValidator->validate(
            $userDataValidateRequestTransfer
        );

        if ($userDataValidationResponse->hasSuccess() === false) {
            $userAddResponseTransfer = new UserAddResponseTransfer();
            $userAddResponseTransfer->setSuccess(false);
            $userAddResponseTransfer->setMessage(
                $userDataValidationResponse->getMessage()
            );

            return $userAddResponseTransfer;
        }

        $userAddResponseTransfer = $this->userWriteRepository->add($userAddRequestTransfer);

        return $userAddResponseTransfer;
    }

    /**
     * @param UserAddRequestTransfer $userAddRequestTransfer
     * 
     * @return UserDataValidateRequestTransfer 
     */
    private function createUserDataValidateRequestTransfer(
        UserAddRequestTransfer $userAddRequestTransfer
    ): UserDataValidateRequestTransfer {
        return (new UserDataValidateRequestTransfer)
            ->setUserName($userAddRequestTransfer->getUserName())
            ->setPassword($userAddRequestTransfer->getPassword())
            ->setCpassword($userAddRequestTransfer->getCpassword())
            ->setRoles($userAddRequestTransfer->getRoles());
    }
}
