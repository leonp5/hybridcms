<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\AdminUpdateUser;

use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\User\Transfer\UserTransfer;
use Leonp5\Hybridcms\User\Transfer\AdminUserUpdateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\AdminUserUpdateResponseTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Persistence\UserWriteRepositoryInterface;
use Leonp5\Hybridcms\User\Business\Processor\UserDataValidation\UserDataValidationProcessorInterface;

class AdminUpdateUserProcessor implements AdminUpdateUserProcessorInterface
{
    /**
     * @var UserDataValidationProcessorInterface
     */
    private UserDataValidationProcessorInterface $userDataValidator;

    /**
     * @var UserWriteRepositoryInterface
     */
    private UserWriteRepositoryInterface $userWriteRepository;

    /**
     * @param UserDataValidationProcessorInterface $userDataValidator
     * @param UserWriteRepositoryInterface $userWriteRepository
     * 
     * @return void 
     */
    public function __construct(
        UserDataValidationProcessorInterface $userDataValidator,
        UserWriteRepositoryInterface $userWriteRepository
    ) {
        $this->userDataValidator = $userDataValidator;
        $this->userWriteRepository = $userWriteRepository;
    }

    /**
     * @param AdminUserUpdateRequestTransfer $adminUserUpdateRequestTransfer
     * 
     * @return AdminUserUpdateResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function update(AdminUserUpdateRequestTransfer $adminUserUpdateRequestTransfer): AdminUserUpdateResponseTransfer
    {
        $adminUserUpdateResponseTransfer = new AdminUserUpdateResponseTransfer();
        $userDataValidateRequestTransfer = $this->createUserDataValidateRequestTransfer($adminUserUpdateRequestTransfer);

        $userDataValidationResponse = $this->userDataValidator->validateForUserUpdate(
            $userDataValidateRequestTransfer
        );

        if ($userDataValidationResponse->hasSuccess() === false) {
            $adminUserUpdateResponseTransfer = new AdminUserUpdateResponseTransfer();
            $adminUserUpdateResponseTransfer->setSuccess(false);
            $adminUserUpdateResponseTransfer->setMessage(
                $userDataValidationResponse->getMessage()
            );

            return $adminUserUpdateResponseTransfer;
        }

        $userTransfer = $this->createUserTransfer($adminUserUpdateRequestTransfer);

        $updateResponse = $this->userWriteRepository->update($userTransfer);

        if ($updateResponse === true) {
            $adminUserUpdateResponseTransfer->setMessage(__('hybridcms::admin.update.data.success'));
        };

        return $adminUserUpdateResponseTransfer;
    }

    /**
     * @param AdminUserUpdateRequestTransfer $adminUserUpdateRequestTransfer
     * 
     * @return UserTransfer 
     */
    private function createUserTransfer(
        AdminUserUpdateRequestTransfer $adminUserUpdateRequestTransfer
    ): UserTransfer {
        return (new UserTransfer)
            ->setPassword($adminUserUpdateRequestTransfer->getPassword())
            ->setCpassword($adminUserUpdateRequestTransfer->getCpassword())
            ->setUserName($adminUserUpdateRequestTransfer->getUserName())
            ->setRoles($adminUserUpdateRequestTransfer->getRoles())
            ->setUserUuid($adminUserUpdateRequestTransfer->getUserUuid())
            ->setUser($adminUserUpdateRequestTransfer->getUser());
    }

    /**
     * @param AdminUserUpdateRequestTransfer $adminUserEditRequestTransfer
     * 
     * @return UserDataValidateRequestTransfer 
     */
    private function createUserDataValidateRequestTransfer(
        AdminUserUpdateRequestTransfer $adminUserEditRequestTransfer
    ): UserDataValidateRequestTransfer {
        return (new UserDataValidateRequestTransfer)
            ->setUserName($adminUserEditRequestTransfer->getUserName())
            ->setPassword($adminUserEditRequestTransfer->getPassword())
            ->setCpassword($adminUserEditRequestTransfer->getCpassword())
            ->setRoles($adminUserEditRequestTransfer->getRoles())
            ->setUserUuid($adminUserEditRequestTransfer->getUserUuid());
    }
}
