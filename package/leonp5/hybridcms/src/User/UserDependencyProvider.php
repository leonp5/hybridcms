<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\User\Persistence\UserReadRepository;
use Leonp5\Hybridcms\User\Persistence\UserWriteRepository;
use Leonp5\Hybridcms\User\Persistence\UserReadRepositoryInterface;
use Leonp5\Hybridcms\User\Persistence\UserWriteRepositoryInterface;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\User\Business\Processor\AdminAddUser\AdminAddUserProcessor;
use Leonp5\Hybridcms\User\Business\Processor\RoleValidation\RoleValidationProcessor;
use Leonp5\Hybridcms\User\Business\Processor\AdminUpdateUser\AdminUpdateUserProcessor;
use Leonp5\Hybridcms\User\Business\Processor\AdminAddUser\AdminAddUserProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\PasswordValidation\PasswordValidationProcessor;
use Leonp5\Hybridcms\User\Business\Processor\UserDataValidation\UserDataValidationProcessor;
use Leonp5\Hybridcms\User\Business\Processor\UserNameValidation\UserNameValidationProcessor;
use Leonp5\Hybridcms\User\Business\Processor\RoleValidation\RoleValidationProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\AdminUpdateUser\AdminUpdateUserProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\UserDataValidation\UserDataValidationProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\PasswordValidation\PasswordValidationProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\UserNameValidation\UserNameValidationProcessorInterface;;

final class UserDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(
            PasswordValidationProcessorInterface::class,
            PasswordValidationProcessor::class
        );
        $app->bind(
            UserNameValidationProcessorInterface::class,
            UserNameValidationProcessor::class
        );
        $app->bind(
            RoleValidationProcessorInterface::class,
            RoleValidationProcessor::class
        );
        $app->bind(
            UserWriteRepositoryInterface::class,
            UserWriteRepository::class
        );
        $app->bind(
            UserReadRepositoryInterface::class,
            UserReadRepository::class
        );

        $app->bind(
            UserDataValidationProcessorInterface::class,
            function ($app) {
                return new UserDataValidationProcessor(
                    $app->make(PasswordValidationProcessorInterface::class),
                    $app->make(UserNameValidationProcessorInterface::class),
                    $app->make(RoleValidationProcessorInterface::class),
                );
            }
        );

        $app->bind(AdminAddUserProcessorInterface::class, function ($app) {
            return new AdminAddUserProcessor(
                $app->make(UserDataValidationProcessorInterface::class),
                $app->make(UserWriteRepositoryInterface::class)
            );
        });

        $app->bind(AdminUpdateUserProcessorInterface::class, function ($app) {
            return new AdminUpdateUserProcessor(
                $app->make(UserDataValidationProcessorInterface::class),
                $app->make(UserWriteRepositoryInterface::class)
            );
        });
    }
}
