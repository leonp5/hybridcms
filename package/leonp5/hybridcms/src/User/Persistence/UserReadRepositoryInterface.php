<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Persistence;

use Illuminate\Support\Collection;
use Leonp5\Hybridcms\User\Transfer\UserTransfer;

interface UserReadRepositoryInterface
{
    /**
     * @return Collection 
     */
    public function findAllUsers(): Collection;

    /**
     * @return Collection
     */
    public function findAllUserRoles(): Collection;

    /**
     * @return Collection 
     */
    public function findAllRoles(): Collection;

    /**
     * @param string $userUuid
     * 
     * @return UserTransfer 
     */
    public function findUserByUuid(string $userUuid): UserTransfer;

    /**
     * @param int $userId
     * 
     * @return Collection
     */
    public function findRolesByUserId(int $userId): Collection;
}
