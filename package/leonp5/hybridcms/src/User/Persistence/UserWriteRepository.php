<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Persistence;

use Throwable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Leonp5\Hybridcms\User\Transfer\UserTransfer;
use Leonp5\Hybridcms\User\Transfer\UserAddRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserAddResponseTransfer;

final class UserWriteRepository implements UserWriteRepositoryInterface
{

    /**
     * @var DB
     */
    private DB $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @param UserAddRequestTransfer $userAddRequestTransfer
     * 
     * @return UserAddResponseTransfer 
     */
    public function add(UserAddRequestTransfer $userAddRequestTransfer): UserAddResponseTransfer
    {
        $userAddResponseTransfer = new UserAddResponseTransfer();

        try {
            $userId = $this->db::table('users')
                ->insertGetId(
                    [
                        'password' => Hash::make($userAddRequestTransfer->getPassword()),
                        'username' => $userAddRequestTransfer->getUserName(),
                        'uuid' => Str::uuid(),
                        'created_At' => Carbon::now()
                    ]
                );
        } catch (Throwable $e) {
            $userAddResponseTransfer->setSuccess(false);
            $userAddResponseTransfer->setMessage($e->getMessage());
            return $userAddResponseTransfer;
        }

        $roleData = [];
        foreach ($userAddRequestTransfer->getRoles() as $key => $value) {
            $roleData[] = [
                'role_id' => $value,
                'user_id' => $userId
            ];
        }

        $this->db::table('role_user')
            ->insert($roleData);

        $msg = __('hybridcms::user.admin.save.user.success');
        $userAddResponseTransfer->setMessage($msg);

        return $userAddResponseTransfer;
    }

    /**
     * @param UserTransfer $userTransfer
     * 
     * @return bool 
     * @throws MassAssignmentException 
     */
    public function update(UserTransfer $userTransfer): bool
    {
        try {
            $user = $userTransfer->getUser();

            $user->roles()->sync($userTransfer->getRoles());

            //Save user
            if ($userTransfer->getUserName()) {
                $user->username = $userTransfer->getUserName();
            }

            if ($userTransfer->getPassword()) {
                $user->password = Hash::make($userTransfer->getPassword());
            }

            if ($user->save()) {
                return true;
            }
        } catch (Throwable $e) {
            return false;
        }

        return true;
    }
}
