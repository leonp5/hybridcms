<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Persistence;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Leonp5\Hybridcms\User\Transfer\UserTransfer;

final class UserReadRepository implements UserReadRepositoryInterface
{

    /**
     * @var DB
     */
    private DB $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @return Collection 
     */
    public function findAllUsers(): Collection
    {
        return $this->db::table('users')
            ->select(['users.id', 'users.uuid', 'users.username', 'users.last_online_at'])
            ->get();
    }

    /**
     * @return Collection 
     */
    public function findAllUserRoles(): Collection
    {
        return $this->db::table('role_user')
            ->join('roles', 'roles.id', '=', 'role_user' . '.role_id')
            ->select(['role_user.role_id', 'role_user.user_id', 'roles.role'])
            ->get();
    }

    /**
     * @return Collection
     */
    public function findAllRoles(): Collection
    {
        return $this->db::table('roles')
            ->get(['roles.id', 'roles.role'])
            ->sortBy('role');
    }

    /**
     * @param string $userUuid
     * 
     * @return UserTransfer 
     */
    public function findUserByUuid(string $userUuid): UserTransfer
    {
        $userTransfer = new UserTransfer();

        $user = $this->db::table('users')
            ->select(['users.id', 'users.uuid', 'users.username'])
            ->where('users.uuid', '=', $userUuid)
            ->first();

        $userTransfer->setUserId($user->id);
        $userTransfer->setUserUuid($user->uuid);
        $userTransfer->setUserName($user->username);

        return $userTransfer;
    }

    /**
     * @param int $userId
     * 
     * @return Collection 
     */
    public function findRolesByUserId(int $userId): Collection
    {
        return $this->db::table('role_user')
            ->select(['role_user.role_id', 'role_user.user_id'])
            ->where('role_user.user_id', '=', $userId)
            ->get();
    }
}
