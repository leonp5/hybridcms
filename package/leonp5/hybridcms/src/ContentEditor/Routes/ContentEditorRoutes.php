<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\ContentEditor\Controller\ContentEditorController;

class ContentEditorRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::prefix('fields/editor')->middleware('auth')->group(function () {
            Route::get('/{contentFieldId}/{langKey}', [ContentEditorController::class, 'edit'])
                ->name('field.edit.editor');
            Route::post('/save', [ContentEditorController::class, 'store'])
                ->name('field.save.editor');
        });
    }
}
