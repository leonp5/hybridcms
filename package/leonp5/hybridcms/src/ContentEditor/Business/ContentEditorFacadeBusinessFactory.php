<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Business;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\ContentEditor\Persistence\ContentEditorWriteRepositoryInterface;

class ContentEditorFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return ContentEditorWriteRepositoryInterface
     * @throws BindingResolutionException
     */
    public function getContentEditorWriteRepository(): ContentEditorWriteRepositoryInterface
    {
        return $this->app->make(ContentEditorWriteRepositoryInterface::class);
    }
}
