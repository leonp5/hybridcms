<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Controller;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\Content\Business\ContentFacadeInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Language\Transfer\FindLanguageRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataRequestTransfer;
use Leonp5\Hybridcms\ContentEditor\Business\ContentEditorFacadeInterface;
use Leonp5\Hybridcms\ContentEditor\Transfer\UpdateFieldContentRequestTransfer;
use Leonp5\Hybridcms\ContentEditor\Business\Validation\StoreContentEditorFieldRequest;

class ContentEditorController extends Controller
{

    /**
     * @param int $contentFieldId
     * @param string $langKey
     * @param ContentFacadeInterface $contentFacade
     * @param LanguageFacadeInterface $languageFacade
     * 
     * @return View|Factory
     * @throws BindingResolutionException
     */
    public function edit(
        int $contentFieldId,
        string $langKey,
        ContentFacadeInterface $contentFacade,
        LanguageFacadeInterface $languageFacade,
    ) {
        $contentFielDataRequest = (new ContentFieldDataRequestTransfer())
            ->setContentFieldId($contentFieldId)
            ->setLangKey($langKey);

        $findLanguageRequestTransfer = (new FindLanguageRequestTransfer())
            ->setLangKey($langKey)
            ->setWantedLangAlpha_2($langKey);

        $findLanguageResponse = $languageFacade->findLanguage($findLanguageRequestTransfer);

        $contentFieldDataResponseTransfer = $contentFacade->getContentFieldData(
            $contentFielDataRequest
        );

        return view(
            'hybridcms::backend.user.pages.partials.field-editor',
            [
                'contentField' => $contentFieldDataResponseTransfer,
                'language' => $findLanguageResponse
            ]
        );
    }

    /**
     * @param StoreContentEditorFieldRequest $request
     * @param ContentEditorFacadeInterface $contentEditorFacade
     * 
     * @return RedirectResponse
     * @throws BindingResolutionException
     */
    public function store(
        StoreContentEditorFieldRequest $request,
        ContentEditorFacadeInterface $contentEditorFacade
    ): RedirectResponse {
        $storeFieldContentRequestTransfer = (new UpdateFieldContentRequestTransfer)
            ->setLangKey($request->input('language'))
            ->setFieldId((int) $request->input('field_id'))
            ->setFieldContent($request->input('field_content'));

        $response = $contentEditorFacade->updateFieldContent($storeFieldContentRequestTransfer);

        if ($response->hasSuccess() === false) {
            return back()->with('error', $response->getMessage());
        }

        return back()->with('success', $response->getMessage());
    }
}
