<?php

namespace Leonp5\Hybridcms\Auth\Controller;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Leonp5\Hybridcms\App\Controller\Controller;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
}
