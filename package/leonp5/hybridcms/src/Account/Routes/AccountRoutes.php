<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Account\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\Account\Controller\AccountController;

class AccountRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::get('/', [AccountController::class, 'welcome'])->name('welcome');
    }
}
