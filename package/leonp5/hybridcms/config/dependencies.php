<?php

declare(strict_types=1);

use Leonp5\Hybridcms\User\UserDependencyProvider;
use Leonp5\Hybridcms\Page\PageDependencyProvider;
use Leonp5\Hybridcms\Content\ContentDependencyProvider;
use Leonp5\Hybridcms\Utility\UtilityDependencyProvider;
use Leonp5\Hybridcms\Language\LanguageDependencyProvider;
use Leonp5\Hybridcms\Frontend\FrontendDependencyProvider;
use Leonp5\Hybridcms\Template\TemplateDependencyProvider;
use Leonp5\Hybridcms\ContentField\ContentFieldDependencyProvider;
use Leonp5\Hybridcms\ContentEditor\ContentEditorDependencyProvider;

return [
    UtilityDependencyProvider::class,
    UserDependencyProvider::class,
    LanguageDependencyProvider::class,
    FrontendDependencyProvider::class,
    ContentEditorDependencyProvider::class,
    ContentDependencyProvider::class,
    TemplateDependencyProvider::class,
    PageDependencyProvider::class,
    ContentFieldDependencyProvider::class,
];
