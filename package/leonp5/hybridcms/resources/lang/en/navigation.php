<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | navigation.
    |
    */

    'pages' => 'Pages',
    'pages.overview' => 'Pages overview',
    'pages.create.new' => 'Create new page',

    'user.management' => 'User management',
    'users.overview' => 'All users',
    'users.create' => 'Add new user',

    'languages' => 'Languages',
    'languages.settings' => 'Language settings',

    'templates.settings' => 'Template Settings',

    'user.dropdown.settings' => 'Settings',
    'user.dropdown.account' => 'Account',
    'user.dropdown.user' => 'User',

    'breadcrumb.dashboard' => 'Dashboard',
    'breadcrumb.pages.overview' => 'Pages overview',
    'breadcrumb.pages.new' => 'New page',
    'breadcrumb.pages.edit' => 'Edit page',

    'breadcrumb.users.overview' => 'User overview',
    'breadcrumb.users.new' => 'New user',
    'breadcrumb.users.edit' => 'Edit user',

    'breadcrumb.languages' => 'Language settings',

    'breadcrumb.templates' => 'Template settings',
    'breadcrumb.templates.fields' => 'Template fields',
];
