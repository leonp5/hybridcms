<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Frontend Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for all translations concerning the frontend.
    |
    */

    'page.preview.text' => 'This is for testing',
];
