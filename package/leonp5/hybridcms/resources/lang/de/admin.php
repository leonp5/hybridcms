<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | admin dashboard and actions.
    |
    */

    // General
    'update.data.success' => 'Änderungen erfolgreich gespeichert!',

    // Pages table
    'create.new.page' => 'Neue Seite anlegen',
    'page.create' => 'Seite anlegen',
    'page.select.template' => 'Template auswählen',
    'page.select.template.abort' => 'Abbrechen',
    'page.select.template.preview' => 'Template Vorschau',
    'page.select.template.preview.unavailable' => 'Keine Template Vorschau möglich',
    'page.status.draft' => 'Entwurf',
    'page.status.inactive' => 'Deaktiviert',
    'page.status.published' => 'Veröffentlicht',
    'page.no-pages' => 'Es gibt noch keine Seiten',
    'page.title.missing' => 'Titel fehlt',
    'page.url.missing' => 'URL fehlt',

    // Page create, edit & delete

    'delete.modal.title' => 'Löschvorgang bestätigen',
    'delete.modal.text' => 'Seite unwiderruflich löschen?',
    'delete.abort' => 'Abbrechen',
    'delete.page.confirm' => 'Seite löschen',

    'page.update.success' => 'Seite aktualisiert!',
    'page.save.success' => 'Seite gespeichert!',

    'page.delete.success' => 'Seite gelöscht!',

    // url error messages
    'invalid.url' => 'Ungültige URL',
    'url.already.exists' => 'Die URL existiert bereits in der Sprache ":language"',
    'min.one.url' => 'Die Seite braucht in mindestens einer Sprache eine gültige URL',
    'page.need.different.urls' => 'Die URL darf nicht mit der URL dieser Seite in einer anderen Sprache identisch sein',

    'page.missing.title.min' => 'Die Seite braucht in mindestens einer Sprache einen Titel',

    // Languages
    'languages.multilang.disabled' => 'Mehrsprachigkeit nicht aktiviert',
    'languages.active.heading' => 'Aktivierte Sprachen',
    'languages.all.heading' => 'Alle Sprachen',
    'languages.settings' => 'Spracheinstellungen',
    'languages.table.heading.language' => 'Sprache',
    'languages.table.heading.main-language' => 'Hauptsprache',
    'languages.table.heading.disable' => 'Deaktivieren',
    'languages.table.heading.delete' => 'Löschen',
    'languages.table.button.update' => 'Speichern',
    'languages.update.one.main.language.error' => 'Es muss genau eine Sprache als Hauptsprache gesetzt sein!',
    'languages.update.language.all.deleted.error' => 'Es dürfen nicht alle Sprachen gelöscht werden!',
    'languages.update.language.all.disabled.error' => 'Es dürfen nicht alle Sprachen deaktiviert werden!',
    'languages.update.success' => 'Spracheinstellungen erfolgreich aktualisiert!',
    'languages.update.error' => 'Aktualisierung der Spracheinstellungen fehlgeschlagen!',
    'languages.delete.warning' => 'Löscht alle Websiteninhalte der ausgewählten Sprache. Deaktivieren der Sprache empfohlen!',

    // Templates
    'templates.settings.heading' => 'Template Einstellungen',
    'templates.scan' => 'Template Dateien einlesen',
    'templates.field.already.exist.error' => '":fieldName" existiert bereits im Template!',
    'templates.scan.update.success' => 'Template scan & update erfolgreich!',
    'templates.data.attribute.missing.error' => 'In ":template" fehlt das Attribut ":attribute"!',
    'templates.overview.tab.all-templates' => 'Alle Templates',
    'templates.overview.tab.single-use' => 'Einmal verwendbar',
    'templates.overview.tab.components' => 'Komponenten',
    'templates.overview.tab.partials' => 'Teil Templates',
    'templates.table.heading.name' => 'Name',
    'templates.table.heading.partial' => 'Teil Template',
    'templates.table.heading.single-use' => 'Einmal verwendbar',
    'templates.table.heading.component' => 'Komponente',
    'templates.table.heading.disabled' => 'Deaktiviert',
    'templates.scan.not-happened' => 'Die Template Dateien wurden noch nicht eingelesen',
    'templates.status.update.error' => 'Status Update fehlgeschlagen!',
    'template.fields.table.heading.name' => 'Feldname',
    'template.fields.table.heading.template-id' => 'Template Id',
    'template.fields.table.heading.type' => 'Typ',
    'template.fields.table.type-info' => 'Reines Textfeld oder mit Editor.',
    'template.fields.no-fields' => 'Keine Template Felder gefunden!',
    'template.delete.modal.title' => 'Unbenutze Templates',
    'template.delete.modal.confirm' => 'Templates löschen',
    'template.delete.modal.keep' => 'Templates behalten',
    'template.delete.success' => 'Templates aus der Datenbank entfernt und HTML/Blade Dateien gelöscht.',
    'template.delete.templates.missing.error' => 'Es wurden keine Templates zum Löschen ausgewählt!',
    'template.scan.settings.unused' => 'Unbenutzt',
    'template.scan.settings.partials' => 'Scan Teil Templates',
    'template.scan.settings.components' => 'Scan Komponenten',
    'template.scan.unused.tooltip' => 'Sucht nach nicht verwendeten Templates und zeigt diese nach dem Scan an.',
];
