<nav aria-label="breadcrumb">
    <ol class="breadcrumb border-0 mt-1">
        @isset ($items)
            @foreach ($items as $item)
                @if(array_key_exists('url', $item))
                <li class="breadcrumb-item">
                    <a href="{{ $item['url'] }}">{{ $item['name'] }}</a>
                </li>
                @else
                <li class="breadcrumb-item active">
                    {{ $item['name'] }}
                </li>
                @endif
            @endforeach
        @endisset
        @isset ($buttons)
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                <a class="btn" href="#"><i class="cil-speech"></i></a>
                <a class="btn" href="#"><i class="cil-graph mr-2"></i> Dashboard</a>
                <a class="btn" href="#"><i class="cil-settings mr-2"></i> Settings</a>
                </div>
            </li>
      @endisset
    </ol>
</nav>