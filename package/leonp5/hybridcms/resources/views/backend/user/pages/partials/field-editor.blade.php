@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.field.edit', ['fieldName' => $contentField->getFieldName()]))

@section('styles')
    @if (env('FRONTEND_MULTILANG') === true)
        <link href="{{ asset('assets/backend/flags.css') }}" rel="stylesheet" type="text/css">
    @endif
    <link href="{{ asset('assets/backend/suneditor/suneditor.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('breadcrumb')
@include('hybridcms::backend.layout.partials.breadcrumb',
['items' => [
['url' => route('dashboard'), 'name' => __('hybridcms::navigation.breadcrumb.dashboard')],
['url' => route('pages.index'), 'name' => __('hybridcms::navigation.breadcrumb.pages.overview')],
['url' => route('pages.edit', ['page' => $contentField->getPageId()]), 'name' =>
__('hybridcms::navigation.breadcrumb.pages.edit')],
['name' => $contentField->getFieldName()]
]])
@endsection

@section('content')
<div class="container">

    <div class="d-flex justify-content-between mb-3">
        <h5 class="text-info">{{$contentField->getFieldName()}}</h5>
        @if (env('FRONTEND_MULTILANG') === true)
        <div class="d-flex align-items-center">
            <h5 class="mb-0 mr-1">{{$language->getLangName()}}</h5>
            <span class="flag-icon flag-icon-{{$language->getAlpha_2()}}"></span>
        </div>
        @endif
    </div>

    <div class="d-flex flex-column position-relative">
        <div class="hcms-position-absolute-center" id="hcms-editor-spinner">
            <div class="spinner-border text-dark" role="status">
                <span class="sr-only"></span>
            </div>
        </div>
        <form class="fade hcms-vh-75" id="hcms-content-field-edit" method="POST" action="{{route('field.save.editor')}}">
            @csrf
            <textarea class="d-none" data-hcms-vendor-function="JsContentEditor" name="field_content"
            id="{{$contentField->getFieldId()}}">{{$contentField->getContentData()}}</textarea>
            <input type="hidden" name="language" value="{{$language->getAlpha_2()}}">
            <input type="hidden" name="field_id" value="{{$contentField->getFieldId()}}">
        </form>

        <div class="form-group d-flex justify-content-between mt-3">
            <a href="{{ url()->previous() }}"
                class="btn btn-secondary">{{__('hybridcms::pages.content.field.editor.abort')}}</a>
            <button form="hcms-content-field-edit" type="submit" id="hcms-content-update-submit"
                class="btn btn-primary">{{__('hybridcms::pages.content.field.editor.submit')}}</button>
        </div>


        <a target="_blank" href="http://suneditor.com/sample/index.html"
            class="text-muted text-center mt-3 mb-2">{{__('hybridcms::pages.js.editor.attribution', ['editorName' => 'Suneditor'])}}</a>

    </div>
</div>
@endsection