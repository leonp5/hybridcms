@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.page.preview', ['pageTitle' => $pageTitle]))

@section('content')

<div class="container-fluid p-0">

    <div class="d-none" id="hcms-field-ids">
        @foreach ($fieldIds as $key => $fieldId)
        <input type="hidden" name="{{$key}}" value="{{$fieldId}}">
        @endforeach
    </div>

    <iframe id="{{$pageId}}" class="hcms-page-preview" srcdoc="{{ $pagePreview }}" data-hcms-function="PagePreview">

    </iframe>

    @include('hybridcms::backend.user.pages.partials.field-edit-modal')

</div>

@endsection