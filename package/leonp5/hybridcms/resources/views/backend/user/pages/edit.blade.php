@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.page.edit'))

@include('hybridcms::backend.layout.partials.flags-stylesheet')

@section('content')

@section('breadcrumb')
    @include('hybridcms::backend.layout.partials.breadcrumb', 
        ['items' => [
        ['url' => route('dashboard'), 'name' => __('hybridcms::navigation.breadcrumb.dashboard')],
        ['url' => route('pages.index'), 'name' => __('hybridcms::navigation.breadcrumb.pages.overview')],
        ['name' => __('hybridcms::navigation.breadcrumb.pages.edit')]]
        ])
@endsection

<div class="container" data-hcms-function="DescriptionPreview">
    <h3 class="mb-4">{{__('hybridcms::pages.edit.page')}}</h3>

    <form action="{{route('pages.update', ['pageId' => $page->id])}}" method="POST">
        @method('PUT')

        @include('hybridcms::backend.user.pages.partials.fields')
        
        <div class="form-group d-flex justify-content-between">
            <a href="{{ url()->previous() }}" class="btn btn-secondary">{{__('hybridcms::pages.edit.abort')}}</a>
            <button type="submit" class="btn btn-primary">{{__('hybridcms::pages.edit.submit')}}</button>
        </div>
    </form>

</div>

@endsection