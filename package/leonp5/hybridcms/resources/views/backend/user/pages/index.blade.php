@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.pages.index'))

@section('breadcrumb')
    @include('hybridcms::backend.layout.partials.breadcrumb', 
        ['items' => [
        ['name' => __('hybridcms::navigation.breadcrumb.pages.overview')],
        ]])
@endsection

@section('content')

<div class="container">
  @canany(['admin', 'editor'])
  <button class="btn btn-primary mb-3" data-hcms-create-page-modal-open=""
    data-hcms-target="hcms-page-create-modal">{{__('hybridcms::admin.create.new.page')}}</button>
  @endcanany
  @if(count($allPages) > 0)
  <table class="table table-responsive-sm">
    <thead>
      <tr>
        <th scope="col">{{__('hybridcms::pages.table.heading.page.title')}}</th>
        <th scope="col">URL</th>
        <th scope="col">Status</th>

        @if (env('FRONTEND_MULTILANG') === true)
        <th scope="col">{{__('hybridcms::pages.table.heading.available.lang')}}</th>
        @endif

        @canany(['admin', 'editor'])
        <th scope="col"></th>
        @endcanany

        @can('admin')
        <th scope="col">{{__('hybridcms::pages.table.heading.deletable')}}</th>
        @endcan
      </tr>
    </thead>
    <tbody>
      @foreach ($allPages as $page)
      <tr>

        <td>
          <a href="{{route('pages.edit', ['page' => $page->id])}}">
            {{$page->title !== null ? $page->title : __('hybridcms::admin.page.title.missing')}}</a>
            <a href="{{route('pages.page.preview', ['page' => $page->id, 'langKey' => app()->getLocale()])}}">
              <x-hybridcms-svg svg="eye" class="bi bi-eye ml-1" 
              viewBox="16 16" width="16" height="16" fill=""/>
            </a>
        </td>

        <td>{{ $page->url !== null ? $page->url : __('hybridcms::admin.page.url.missing')}}</td>

        @if($page->page_status_id === 
        constant('Leonp5\Hybridcms\Page\Business\Status\PageStatusInterface::PAGE_STATUS_DRAFT'))
        <td>
          <span class="badge badge-warning">
            {{__('hybridcms::admin.page.status.draft')}}
          </span></td>
        @elseif ($page->page_status_id === 
        constant('Leonp5\Hybridcms\Page\Business\Status\PageStatusInterface::PAGE_STATUS_INACTIVE'))
        <td>
          <span class="badge badge-secondary">
            {{__('hybridcms::admin.page.status.inactive')}}
          </span></td>
        @elseif($page->page_status_id === 
        constant('Leonp5\Hybridcms\Page\Business\Status\PageStatusInterface::PAGE_STATUS_PUBLISHED'))
        <td>
          <span class="badge badge-success">{{__('hybridcms::admin.page.status.published')}}
          </span></td>
        @endif

        @if (env('FRONTEND_MULTILANG') === true)
        <td>{{$page->available_lang}}</td>
        @endif

        <td>
          @canany(['admin', 'editor'])
          @if($page->deletable == true)
          <a type="button" data-toggle="modal" data-target="{{'#deleteConfirmModal' . $page->id}}">
            <x-hybridcms-svg svg="trash" fill="none" class="svg-delete" stroke-width="2" stroke="currentColor"/>
        </td>
        </a>
        <div class="modal fade" id="{{'deleteConfirmModal'. $page->id}}" tabindex="-1"
          aria-labelledby="deleteConfirmModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteConfirmModalLabel">{{__('hybridcms::admin.delete.modal.title')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row m-2"> {{__('hybridcms::admin.delete.modal.text')}}</div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-auto"
                  data-dismiss="modal">{{__('hybridcms::admin.delete.abort')}}</button>
                <form method="POST" action="{{route('pages.destroy', [$page->id])}}">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit"
                    class="btn btn-danger m-2">{{__('hybridcms::admin.delete.page.confirm')}}</button>
                </form>
              </div>
            </div>
          </div>
          @else

          @endif
          @endcanany
          </td>
          @can('admin')
          <td>
            <div class="custom-control custom-switch hcms-switch-info">
              <form id="deletable_status_{{$page->id}}" method="POST" action="{{route('pages.toggleDeletableStatus')}}">
                <input type="checkbox" class="custom-control-input" id="deletable_{{$page->id}}"
                  {{$page->deletable ? "checked" : ""}} value="{{$page->deletable}}" name="deletable_{{$page->id}}"
                  data-hcms-page-id="{{$page->id}}" data-hcms-function="PageDeletableStatusToggle"
                  form="deletable_status_{{$page->id}}">
                <label class="custom-control-label" for="deletable_{{$page->id}}"></label>
              </form>
            </div>
          </td>
          @endcan
      </tr>
      @endforeach
    </tbody>
  </table>

  {{$allPages->links()}}
  @else
  <div class="d-flex justify-content-center">

    <h4 class='text-info'>{{__('hybridcms::admin.page.no-pages')}}</h5>
  </div>
  @endif
</div>
@endsection