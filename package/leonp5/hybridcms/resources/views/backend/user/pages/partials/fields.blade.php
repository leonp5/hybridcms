@csrf
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <div class="col">
                <h4>{{__('hybridcms::pages.general.page.content')}}</h4>
            </div>
            <div class="col">
                <div class="d-flex justify-content-end">
                    <p class="mr-1">{{__('hybridcms::pages.chosen.template')}}:</p>
                    <p class="text-info">
                        <strong>
                            {{$page->template->template_name}}
                        </strong>
                    </p>

                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="form-row mb-5">
            <div class="col-4">
                <input type="hidden" name="template_id" value="{{$page->template->id}}">
                <label class="label-m" for="page_status">@lang('hybridcms::pages.edit.status')</label>
                <select class="form-control" id="page_status" name="page_status">
                    @if ($page->page_status_id ?? '')

                    @foreach ($page->statusLabels as $status)
                    <option value="{{$status->id}}" @if($status->id == $page->page_status_id) selected="selected"
                        @endif>{{$status->status_label}}</option>
                    @endforeach

                    @else

                    @foreach ($page->statusLabels as $status)
                    <option value="{{$status->id}}" @if(old('page_status')==$status->id) selected="selected" @endif>
                        {{$status->status_label}}</option>
                    @endforeach

                    @endif
                </select>
            </div>
        </div>
        <div class="nav-tabs-boxed">
            <ul class="nav nav-tabs" role="tablist">
                @foreach($page->activeLanguages as $activeLang)
                <li class="nav-item">

                    @if (env('FRONTEND_MULTILANG') === true)
                    <a class="nav-link {{$loop->first ? 'active' : ''}}" data-toggle="tab"
                        href="#tab_{{$activeLang->alpha_2}}" role="tab" aria-controls="{{$activeLang->alpha_2}}"
                        aria-selected="{{$loop->first ? 'true' : 'false'}}">
                        <span class="flag-icon flag-icon-{{$activeLang->alpha_2}}"></span>
                    </a>
                    @endif

                </li>
                @endforeach
            </ul>

            <div class="tab-content">
                @foreach($page->activeLanguages as $activeLang)
                <div id="tab_{{$activeLang->alpha_2}}" class="tab-pane {{$loop->first ? 'active' : ''}}"
                    role="tabpanel">

                    @if (env('FRONTEND_MULTILANG') === true)
                    <div class="d-flex align-items-center">
                        <span class="flag-icon flag-icon-{{$activeLang->alpha_2}} mr-2"></span>
                        <p class="h4 mb-0 text-truncate">{{$activeLang->langName}}</p>
                    </div>
                    @endif

                    <div class="form-group row">
                        <div class="col">
                            <label class="label-m mt-3"
                                for="{{'title_' . $activeLang->alpha_2}}">{{__('hybridcms::pages.edit.title')}}</label>
                            <input type="text" class="form-control" id="{{'title_' . $activeLang->alpha_2}}"
                                name="general_content[{{$activeLang->alpha_2}}][title]" value="{{old('general_content.'.$activeLang->alpha_2 .'.title', 
                                $page->general_content[$activeLang->alpha_2]->title ?? '')}}" />
                        </div>
                        <div class="col">
                            <label class="label-m mt-3"
                                for="{{'url_' . $activeLang->alpha_2}}">{{__('hybridcms::pages.edit.url')}}</label>
                            <input type="text" class="form-control" id="{{'url_' . $activeLang->alpha_2}}"
                                name="general_content[{{$activeLang->alpha_2}}][url]" value="{{old('general_content.' . $activeLang->alpha_2 . '.url',
                                $page->general_content[$activeLang->alpha_2]->url)}}" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label class="label-m mt-3"
                                for="{{'description_' . $activeLang->alpha_2}}">{{__('hybridcms::pages.edit.description')}}</label>
                            <textarea type="text" class="form-control h-75"
                                id="{{'description_' . $activeLang->alpha_2}}"
                                name="general_content[{{$activeLang->alpha_2}}][description]">{{old('general_content.' . $activeLang->alpha_2 . '.description', $page->general_content[$activeLang->alpha_2]->description)}}</textarea>
                            <p data-hcms-preview-word-count="{{'description_' . $activeLang->alpha_2}}"
                                class="text-right">0</p>
                        </div>
                        <div class="col">
                            <label class="label-m mt-3">{{__('hybridcms::pages.edit.description.preview')}}</label>
                            <div class="hcms-description-preview">
                                <h2 data-hcms-preview="{{'title_' . $activeLang->alpha_2}}">Freiheit - Wikipedia</h2>
                                <a
                                    data-hcms-preview="{{'url_' . $activeLang->alpha_2}}">https://de.wikipedia.org/wiki/Freiheit</a>
                                <button>▼</button>
                                <p data-hcms-preview="{{'description_' . $activeLang->alpha_2}}">Freiheit (lateinisch
                                    libertas) wird in
                                    der Regel als die Möglichkeit verstanden, ohne Zwang zwischen
                                    unterschiedlichen Möglichkeiten auszuwählen und...</p>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <div class="col">
                <h4>{{__('hybridcms::pages.page.content')}}</h4>
            </div>
            <div class="col">
                <div class="d-flex justify-content-end">
                    <p class="mr-1">{{__('hybridcms::pages.chosen.template')}}:</p>
                    <p class="text-info">
                        <strong>
                            {{$page->template->template_name}}
                        </strong>
                    </p>

                </div>
            </div>
        </div>

    </div>
    <div class="card-body">
        @if(count($page->templateFields)> 0)
        <div class="nav-tabs-boxed">
            <ul class="nav nav-tabs" role="tablist">
                @foreach($page->templateFields as $templateField)
                <li class="nav-item">
                    <a class="nav-link {{$loop->first ? 'active' : ''}}" data-toggle="tab"
                        href="#tab_{{$templateField->id}}" role="tab" aria-controls="{{$templateField->id}}"
                        aria-selected="{{$loop->first ? 'true' : 'false'}}">
                        {{$templateField->field_name}}
                    </a>
                </li>
                @endforeach
            </ul>

            <div class="tab-content">
                @foreach($page->templateFields as $templateField)
                <div id="tab_{{$templateField->id}}" class="tab-pane {{$loop->first ? 'active' : ''}}" role="tabpanel">
                    @foreach($page->activeLanguages as $activeLang)
                    <div class="form-group">

                        @if (env('FRONTEND_MULTILANG') === true)
                        <div class="d-flex align-items-center mb-2 mt-2">
                            <span class="flag-icon flag-icon-{{$activeLang->alpha_2}} mr-2"></span>
                            <p class="h4 mb-0 text-truncate">{{$activeLang->langName}}</p>
                        </div>
                        @endif

                        @if($templateField->content_type_id ===
                        constant('Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface::CONTENT_TYPE_TEXT'))

                        <input type="text" class="form-control"
                            name="page_content[{{$activeLang->alpha_2}}][{{$templateField->field_name}}][content]"
                            value="{{old('page_content.' . $activeLang->alpha_2 . '.' . $templateField->field_name . '.content', $page->page_content[$activeLang->alpha_2][$templateField->field_name]['content'])}}">

                        @else

                        @if(isset($page->id))

                        <a href="{{route('field.edit.editor', 
                            ['contentFieldId' => $page->page_content[$activeLang->alpha_2][$templateField->field_name]['field_id'],
                            'langKey' => $activeLang->alpha_2
                            ])}}"
                            class="btn btn-outline-info btn-block">
                            {{__('hybridcms::pages.content.field.open.editor')}}
                        </a>

                        @else

                        <p>{{__('hybridcms::pages.page.create.necessary')}}</p>
                        <input type="hidden"
                            name="page_content[{{$activeLang->alpha_2}}][{{$templateField->field_name}}][content]"
                            value="">

                        @endif

                        @endif



                        <input type="hidden"
                            name="page_content[{{$activeLang->alpha_2}}][{{$templateField->field_name}}][content_type_id]"
                            value="{{old($templateField->content_type_id,  $page->page_content[$activeLang->alpha_2][$templateField->field_name]['content_type_id'])}}">
                        <input type="hidden"
                            name="page_content[{{$activeLang->alpha_2}}][{{$templateField->field_name}}][field_id]"
                            value="{{old('',  $page->page_content[$activeLang->alpha_2][$templateField->field_name]['field_id'])}}">
                        <input type="hidden"
                            name="page_content[{{$activeLang->alpha_2}}][{{$templateField->field_name}}][template_field_id]"
                            value="{{old('',  $page->page_content[$activeLang->alpha_2][$templateField->field_name]['template_field_id'])}}">
                    </div>

                    @endforeach

                </div>
                @endforeach
            </div>
        </div>
        @else
        <div class="container">
            <p class="text-muted text-center">{{__('hybridcms::pages.no.template.fields')}}</p>
        </div>
        @endif
    </div>
</div>