<svg
    width="{{ $width }}"
    height="{{ $height }}"
    viewBox="0 0 {{ $viewBox }}"
    fill="{{ $fill }}"
    stroke="{{ $stroke }}"
    stroke-width="{{ $strokeWidth }}"
    stroke-linecap="round"
    stroke-linejoin="round"
    id="{{ $id }}"
    {{ $attributes->merge(['class' => $class])}}
    {{-- class="w-8 h-8" --}}
    >
    @includeIf("hybridcms::backend.svgs.$svg")
</svg>