@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.templates.index'))

@section('styles')

@section('breadcrumb')
    @include('hybridcms::backend.layout.partials.breadcrumb', 
        ['items' => [
        ['name' => __('hybridcms::navigation.breadcrumb.templates')]
        ]])
@endsection

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h3 class="mb-4">{{__('hybridcms::admin.templates.settings.heading')}}</h3>
        </div>
    </div>
    <div class="row align-items-center mb-3">
        <div class="col">
            <form class="" action="{{route('templates.scan')}}" method="GET" id="hcms-template-scan">
                <button class="btn btn-primary mb-3" form="hcms-template-scan" data-hcms-function="TemplateScan">
                    <span class="" role="status" aria-hidden="true"></span>
                    {{__('hybridcms::admin.templates.scan')}}
                </button>
            </form>
        </div>
        <div class="col-1">

            @include('hybridcms::backend.admin.templates.partials.scan-settings')
            
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            @if(count($tabs['allTemplates']['data']) > 0)
            
            @include('hybridcms::backend.admin.templates.partials.template-table')

            @else
            <h4 class="text-center">{{__('hybridcms::admin.templates.scan.not-happened')}}</h4>
            @endif
        </div>
    </div>
</div>
@endsection