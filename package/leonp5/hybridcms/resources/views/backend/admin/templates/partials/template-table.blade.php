<div class="nav-tabs-boxed">
                <ul class="nav nav-tabs" role="tablist">
                    @foreach ($tabs as $tab)
                    <li class="nav-item">
                        <a class="nav-link {{$loop->first ? 'active' : ''}}" data-toggle="tab"
                            href="#tab_{{$tab['number']}}" role="tab" aria-controls="{{$tab['number']}}"
                            aria-selected="{{$loop->first ? 'true' : 'false'}}">{{$tab['name']}}
                            <span class="badge bg-light text-dark ml-1">{{count($tab['data'])}}</span></a>
                    </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach ($tabs as $tab)
                    <div id="tab_{{$tab['number']}}" class="tab-pane {{$loop->first ? 'active' : ''}}" role="tabpanel">
                        <table class="table table-responsive-sm">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col">{{__('hybridcms::admin.templates.table.heading.name')}}</th>
                                    <th scope="col">{{__('hybridcms::admin.templates.table.heading.partial')}}</th>
                                    <th scope="col">{{__('hybridcms::admin.templates.table.heading.single-use')}}</th>
                                    <th scope="col">{{__('hybridcms::admin.templates.table.heading.component')}}</th>
                                    <th scope="col">{{__('hybridcms::admin.templates.table.heading.disabled')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tab['data'] as $template)
                                <tr>
                                    <td>
                                        <a
                                            href={{route('templates.fields', ['templateId' => $template->id])}}>{{$template->template_name}}</a>
                                    </td>
                                    <td>
                                        @if ($template->partial == true)
                                        <x-hybridcms-svg svg="check" class="svg-check" viewBox="512 512" />
                                        @else
                                        <a class="cross"></a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($template->single_use == true)
                                        <x-hybridcms-svg svg="check" class="svg-check" viewBox="512 512" />
                                        @else
                                        <a class="cross"></a>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input"
                                                {{$template->component ? "checked" : ""}}
                                                value="{{$template->component}}"
                                                name="component_{{$template->template_name}}" disabled>
                                            <label class="custom-control-label"
                                                for="component_{{$template->template_name}}"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <form id="{{$template->template_name}}_{{$tab['number']}}" method="POST"
                                            action="{{route('templates.toggleStatus')}}">
                                            <div class="custom-control custom-switch hcms-switch-danger">
                                                <input type="checkbox" class="custom-control-input"
                                                    id="disabled_{{$template->template_name}}_{{$tab['number']}}"
                                                    {{$template->disabled ? "checked" : ""}}
                                                    value="{{$template->disabled}}"
                                                    name="disabled_{{$template->template_name}}_{{$tab['number']}}"
                                                    data-hcms-template-id="{{$template->id}}"
                                                    data-hcms-function="TemplateStatusToggle"
                                                    form="{{$template->template_name}}_{{$tab['number']}}">
                                                <label class="custom-control-label"
                                                    for="disabled_{{$template->template_name}}_{{$tab['number']}}"></label>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="modal fade" id="deleteConfirmModal" tabindex="-1" aria-labelledby="deleteConfirmModalLabel"
                aria-hidden="true">
                <form class="modal-dialog modal-dialog-centered" method="POST" action="{{route('templates.destroy')}}">
                    {{csrf_field()}}

                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteConfirmModalLabel">
                                {{__('hybridcms::admin.template.delete.modal.title')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked id="" value=""
                                    name="unused_templates[]">
                                <label class="custom-control-label" for=""></label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary mr-auto"
                                data-dismiss="modal">{{__('hybridcms::admin.template.delete.modal.keep')}}</button>
                            <button type="submit"
                                class="btn btn-danger m-2">{{__('hybridcms::admin.template.delete.modal.confirm')}}</button>
                        </div>
                    </div>
                </form>
            </div>