<div class="card">
    <h5 class="card-header">{{__('hybridcms::admin.languages.all.heading')}}
        <span class="badge badge-pill badge-primary float-right"
            id="hcms_all_lang_count">{{count($allLanguages)}}</span>
    </h5>
    <div class="card-body">
        <div class="form-group row">
            <div class="col">
                <div class="input-group">
                    <input class="form-control" id="all_lang_search">
                    <span class="input-group-append">
                        <button class="btn" type="button">
                            <x-hybridcms-svg svg="magnifying-glass" viewBox="512 512" />
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="d-flex flex-wrap" id="hcms_all_languages">
            @foreach($allLanguages as $language)
            <div class="col-6 col-md-4 col-lg-3" name="hcms_lang_container">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="lang_{{$language->id}}"
                        {{isset($language->enabled) ? "checked" : ""}}
                        data-hcms-language-id="{{$language->id}}" name="{{$language->name}}"
                        value="{{$language->alpha_2}}">
                        <label class="custom-control-label"
                        for="lang_{{$language->id}}">{{$language->name}}</label>
                        <span class="flag-icon flag-icon-{{$language->alpha_2}}"></span>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>