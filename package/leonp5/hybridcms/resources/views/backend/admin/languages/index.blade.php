@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.languages.index'))

@section('breadcrumb')
    @include('hybridcms::backend.layout.partials.breadcrumb', 
        ['items' => [
        ['name' => __('hybridcms::navigation.breadcrumb.languages')]
        ]])
@endsection

@if(count($activeLanguages) > 0)
@section('styles')
<link href="{{ asset('assets/backend/flags.css') }}" rel="stylesheet">
@endsection
@endif

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h3 class="mb-3">{{__('hybridcms::admin.languages.settings')}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @if(count($activeLanguages) > 0)
            
            @include('hybridcms::backend.admin.languages.partials.active-languages-card')

        </div>
    </div>
    <div class="row">
        <div class="col">

            @include('hybridcms::backend.admin.languages.partials.all-languages-card')

        </div>
    </div>
    @else
    <h4 class="text-center">{{__('hybridcms::admin.languages.multilang.disabled')}}</h4>
    @endif
</div>
@endsection