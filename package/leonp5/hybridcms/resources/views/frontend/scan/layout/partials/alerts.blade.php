<div data-hcms-single-use="true" data-hcms-partial="true" data-hcms-component="false"></div>

{{-- @if(count($errors) > 0)
@foreach($errors->all() as $error)
<div class="warning-modal" onclick="RemoveAlert()">
  {{$error}}
</div>
@endforeach
@endif --}}

@if(session("success"))
<div class="alert-modal" onclick="RemoveAlert()">
  {{session("success")}}
</div>
@endif

@if(session("error"))
<div class="warning-modal" onclick="RemoveAlert()">
  {{session("error")}}
</div>
@endif