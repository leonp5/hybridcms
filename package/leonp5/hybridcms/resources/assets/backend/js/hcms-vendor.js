import '@coreui/coreui'
import Initializer from './vendor/initializer/Initializer'

const initializer = new Initializer()
initializer.init()
