import Quill from './QuillBase'

// Important: Not used. SunEditor is used at the moment

export default class JsEditor {
    constructor() {}

    init(htmlElement) {
        const quillContainerElement = document.createElement('div')
        quillContainerElement.setAttribute('id', 'hcms-quill-container')
        htmlElement.parentNode.insertBefore(
            quillContainerElement,
            htmlElement.nextSibling
        )
        const quillToolbarElement = document.createElement('div')
        quillToolbarElement.setAttribute('id', 'hcms-quill-toolbar')
        htmlElement.parentNode.insertBefore(
            quillToolbarElement,
            htmlElement.nextSibling
        )
        const options = {
            debug: 'info',
            modules: {
                toolbar: [
                    [{ font: [] }],

                    [{ header: [1, 2, 3, 4, 5, 6, false] }],

                    ['bold', 'italic', 'underline', 'strike'],

                    [{ color: [] }, { background: [] }],

                    [{ script: 'sub' }, { script: 'super' }],

                    ['blockquote', 'code-block'],

                    [
                        { list: 'ordered' },
                        { list: 'bullet' },
                        { indent: '-1' },
                        { indent: '+1' },
                    ],

                    [{ direction: 'rtl' }, { align: [] }],

                    ['link', 'image', 'video', 'formula'],

                    ['clean'],
                ],
            },
            placeholder: 'Compose an epic...',
            readOnly: false,
            theme: 'snow',
        }

        const editor = new Quill('#hcms-quill-container', options)
        this.revealEditor(htmlElement)
    }

    revealEditor(textAreaElement) {
        textAreaElement.parentNode.classList.add('show')
        const spinner = document.getElementById('hcms-editor-spinner')
        spinner.classList.add('fade')
        setTimeout(() => {
            spinner.remove()
        }, 250)
    }
}
