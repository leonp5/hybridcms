import { classes } from './classes'

export default class Initializer {
    constructor() {
        this.elementsWithAttributes = Array.from(
            document.querySelectorAll('[data-hcms-vendor-function]')
        )

        this.classes = classes
    }

    init() {
        if (this.elementsWithAttributes.length > 0) {
            this.elementsWithAttributes.forEach((htmlElement) => {
                const className = htmlElement.getAttribute(
                    'data-hcms-vendor-function'
                )
                const classToExecute = this.classes[className]
                classToExecute.init(htmlElement)
            })
        }
    }
}
