export default class LocaleSetter {
    constructor() {
        this.locale = null
        this.set()
    }

    set() {
        if (document.documentElement.lang === null) {
            this.locale = 'gb'
        }

        if (document.documentElement.lang === undefined) {
            this.locale = 'gb'
        }

        this.locale = document.documentElement.lang
    }

    get() {
        return this.locale
    }
}
