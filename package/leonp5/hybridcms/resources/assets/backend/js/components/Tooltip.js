import StringWordCounter from './utils/StringWordCounter'

export default class Tooltip {
    constructor() {
        this.alreadyInitialized = false

        this.hideTooltipKeyOnKeyUp = e => {
            // 27 = 'ESC'-key
            if (e.keyCode === 27 || e.code === 'Escape') {
                this.hideTooltip()
            }

            document.body.removeEventListener(
                'keyup',
                this.hideTooltipKeyOnKeyUp,
                false
            )
        }
    }

    init(htmlElement) {
        if (this.alreadyInitialized === true) {
            return
        }
        this.targetElements = Array.from(
            document.querySelectorAll('[data-hcms-tooltip]')
        )

        this.targetElements.forEach(targetEl => {
            targetEl.addEventListener('mouseover', e => {
                this.showTooltipHover(e)
            })

            targetEl.addEventListener('click', e => {
                this.showTooltipClick(e)
            })

            targetEl.style.cursor = 'pointer'
        })

        this.alreadyInitialized = true
    }

    create(e) {
        const targetElement = e.target
        const tooltip = document.createElement('div')
        tooltip.className = 'hcms-tooltip'
        tooltip.innerHTML = targetElement.getAttribute('data-hcms-tooltip')

        document.body.appendChild(tooltip)

        const position = this.createPosition(targetElement)

        this.posHorizontal = position.split(' ')[0]
        this.posVertical = position.split(' ')[1]

        this.position(targetElement, tooltip, position)

        // remove tooltip
        document.body.addEventListener(
            'keyup',
            this.hideTooltipKeyOnKeyUp,
            false
        )

        document.addEventListener('click', e => {
            this.hideTooltipClick(e)
        })

        document.body.addEventListener('mouseout', e => {
            this.hideTooltipHover(e)
        })
    }

    position(targetElement, tooltip, position) {
        let targetElementCoords = targetElement.getBoundingClientRect()
        let left = targetElementCoords.left
        let top = targetElementCoords.top
        let dist = 10

        // position the tooltip based on HTML data attribute
        switch (position) {
            case 'left':
                left =
                    parseInt(targetElementCoords.left) -
                    dist -
                    tooltip.offsetWidth
                if (
                    parseInt(targetElementCoords.left) - tooltip.offsetWidth <
                    0
                ) {
                    left = dist
                }

                top =
                    (parseInt(targetElementCoords.top) +
                        parseInt(targetElementCoords.bottom)) /
                        2 -
                    tooltip.offsetHeight / 2
                tooltip.classList.add('tooltip-left')
                break

            case 'right':
                left = targetElementCoords.right + dist
                if (
                    parseInt(targetElementCoords.right) + tooltip.offsetWidth >
                    document.documentElement.clientWidth
                ) {
                    left =
                        document.documentElement.clientWidth -
                        tooltip.offsetWidth -
                        dist
                }

                top =
                    (parseInt(targetElementCoords.top) +
                        parseInt(targetElementCoords.bottom)) /
                        2 -
                    tooltip.offsetHeight / 2
                tooltip.classList.add('tooltip-right')
                break

            case 'bottom':
                top = parseInt(targetElementCoords.bottom) + dist
                left =
                    parseInt(targetElementCoords.left) +
                    (targetElement.offsetWidth - tooltip.offsetWidth) / 2
                tooltip.classList.add('tooltip-bottom')
                break

            default:
            case 'top':
                top =
                    parseInt(targetElementCoords.top) -
                    tooltip.offsetHeight -
                    dist
                left =
                    parseInt(targetElementCoords.left) +
                    (targetElement.offsetWidth - tooltip.offsetWidth) / 2
                tooltip.classList.add('tooltip-top')
        }

        left = left < 0 ? parseInt(targetElementCoords.left) : left
        top = top < 0 ? parseInt(targetElementCoords.bottom) + dist : top

        tooltip.style.left = left + 'px'
        tooltip.style.top = top + pageYOffset + 'px'

        // Correct the position, if necessary
        // Position tooltip on the left side of the targetElement if there is no space on the right
        if (
            window.innerWidth - targetElementCoords.right <
                tooltip.offsetWidth &&
            targetElementCoords.width < 50 &&
            position === 'right'
        ) {
            tooltip.style.left =
                targetElementCoords.left -
                (tooltip.offsetWidth + targetElementCoords.width) +
                'px'
            tooltip.className = 'hcms-tooltip tooltip-left'
            // Position tooltip on the right side of the targetElement if there is no space on the left
        } else if (
            targetElementCoords.left < tooltip.offsetWidth &&
            targetElementCoords.width < 50 &&
            position === 'left'
        ) {
            tooltip.style.left =
                targetElementCoords.left + targetElementCoords.width + 20 + 'px'
            tooltip.className = 'hcms-tooltip tooltip-right'
            // Position tooltip on the bottom if the targetElement is too close to the top of the page
        } else if (targetElementCoords.top < tooltip.offsetHeight) {
            tooltip.style.top = top =
                parseInt(targetElementCoords.bottom) + dist
            tooltip.className = 'hcms-tooltip tooltip-bottom'
            // Position tooltip on the top, if the targetElement is too close to the bottom of the page
        } else if (
            window.innerHeight - parseInt(targetElementCoords.bottom) <
            tooltip.offsetHeight
        ) {
            tooltip.style.top =
                parseInt(targetElementCoords.top) - tooltip.offsetHeight - dist
            tooltip.className = 'hcms-tooltip tooltip-top'
        }
    }

    createPosition(targetElement) {
        const position = targetElement.getAttribute('data-tooltip-position')

        if (position === null) {
            return 'top'
        }

        const stringWordCounter = new StringWordCounter(position)
        const hasOneWord = stringWordCounter.hasMax(1)
        if (hasOneWord === false) {
            return 'top'
        }

        const allowedPositions = ['top', 'bottom', 'left', 'right']
        const positionAllowed = allowedPositions.indexOf(position) > -1
        if (positionAllowed === false) {
            return 'top'
        }

        return position
    }

    showTooltipHover(e) {
        this.create(e)
    }

    showTooltipClick(e) {
        this.create(e)
    }

    hideTooltipHover(e) {
        if (e.target.hasAttribute('data-hcms-tooltip')) {
            return
        } else {
            this.hideTooltip()
        }
    }

    hideTooltipClick(e) {
        if (!e.target.hasAttribute('data-hcms-tooltip')) {
            this.hideTooltip()
        }
    }

    hideTooltip() {
        if (document.querySelector('.hcms-tooltip') !== null) {
            document.body.removeChild(document.querySelector('.hcms-tooltip'))
        }
    }
}
