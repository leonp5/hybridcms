import RemoveAlert from './RemoveAlert'

export default class AlertCreator {
    constructor(message, type) {
        this.message = message
        this.type = type
    }

    createAlertElement() {
        const alertElement = document.createElement('div')
        alertElement.addEventListener('click', () => {
            RemoveAlert()
        })
        if (this.type === 'error') {
            alertElement.className = 'warning-modal'
        } else {
            alertElement.className = 'alert-modal'
        }
        alertElement.innerHTML = this.message
        const mainElement = document.getElementsByTagName('main').item(0)
        mainElement.append(alertElement)
    }
}
