import AlertCreator from '../utils/AlertCreator'
import Fetch from '../fetch/Fetch'

export default class PageDeletableStatusToggle {
    constructor() {
        this.switch = null
    }

    init(htmlElement) {
        this.switch = htmlElement

        this.switch.addEventListener('change', e => {
            this.updateStatus(e)
        })
    }

    async updateStatus(e) {
        const pageId = e.target.getAttribute('data-hcms-page-id')
        const fetch = new Fetch()
        fetch.createFetchOptions(this.switch.form, { page_id: pageId })
        const response = await fetch.doFetch()

        if (response.status !== 200) {
            this.handleResponseError(await response.json())
        } else {
            setTimeout(() => {
                location.reload()
            }, 500)
        }
    }

    handleResponseError(response) {
        new AlertCreator(response.error, 'error').createAlertElement()
    }
}
