import AlertCreator from '../utils/AlertCreator'
import Fetch from '../fetch/Fetch'

export default class TemplateScan {
    constructor() {
        this.startScanButton = null
        this.unusedTemplatesModalId = 'deleteConfirmModal'
        this.unusedTemplatesData = null
        this.scanOptionsForm = null
        this.scanOptionSwitches = null
    }

    init(htmlElement) {
        this.startScanButton = htmlElement
        this.startScanButton.addEventListener('click', e => {
            this.doTemplateScan(e)
        })
        this.scanOptionsForm = document.getElementById('hcms-scan-options-form')
        this.scanOptionSwitches = Array.from(
            this.scanOptionsForm.querySelectorAll('input')
        )

        this.scanOptionSwitches.forEach(optionSwitch => {
            optionSwitch.addEventListener('change', e => {
                this.toggleScanOption(e)
            })
        })
    }

    async doTemplateScan(e) {
        e.preventDefault()
        this.addSpinner()
        const fetch = new Fetch()
        fetch.createFetchOptions(this.startScanButton.form)
        const response = await fetch.doFetch()

        if (response.status !== 200) {
            this.handleResponseError(await response.json())
        } else {
            this.handleResponseSuccess(await response.json())
        }
        this.removeSpinner()
    }

    addSpinner() {
        this.startScanButton.firstElementChild.classList =
            'spinner-border spinner-border-sm'
    }

    removeSpinner() {
        setTimeout(() => {
            this.startScanButton.firstElementChild.classList = ''
        }, 500)
    }

    handleResponseSuccess(response) {
        if (response.unusedTemplates != null) {
            this.unusedTemplates = response.unusedTemplates
            this.showUnusedTemplatesModal()
            return
        }
        new AlertCreator(response.success, 'success').createAlertElement()
        setTimeout(() => {
            location.reload()
        }, 1000)
    }

    handleResponseError(response) {
        new AlertCreator(response.error, 'error').createAlertElement()
    }

    showUnusedTemplatesModal() {
        const button = document.createElement('button')
        button.classList.add('d-none')
        button.setAttribute('data-toggle', 'modal')
        button.setAttribute('data-target', '#' + this.unusedTemplatesModalId)
        document.body.append(button)
        this.insertUnusedTemplatesIntoModalBody()
        button.click()
        button.remove()

        const closeButtons = Array.from(
            document
                .getElementById(this.unusedTemplatesModalId)
                .querySelectorAll('button[type="button"]')
        )

        closeButtons.forEach(closeButton => {
            closeButton.addEventListener('click', () => {
                setTimeout(() => {
                    location.reload()
                }, 500)
            })
        })
    }

    insertUnusedTemplatesIntoModalBody() {
        const row = document
            .getElementById(this.unusedTemplatesModalId)
            .querySelector('div[class="custom-control custom-checkbox"]')

        const modalBody = document
            .getElementById(this.unusedTemplatesModalId)
            .querySelector('div[class="modal-body"]')

        this.unusedTemplates.forEach(template => {
            const clonedRow = row.cloneNode(true)
            const rowWithData = this.createRow(clonedRow, template)
            modalBody.append(rowWithData)
        })

        row.remove()
    }

    createRow(row, template) {
        const label = row.querySelector('label[class="custom-control-label"]')
        label.innerHTML = template.template_name
        label.setAttribute('for', 'unused_template_' + template.id)

        const checkbox = row.querySelector('input[type="checkbox"]')
        checkbox.value = template.id
        checkbox.id = 'unused_template_' + template.id

        return row
    }

    async toggleScanOption(e) {
        const fetch = new Fetch()
        fetch.createFetchOptions(e.target.form, {
            option_name: e.target.name
        })
        const response = await fetch.doFetch()

        if (response.status !== 200) {
            this.handleResponseError(await response.json())
        }
    }
}
