<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Content\Business\Processor\ContentTableCreate\ContentTableCreateProcessor;

class CreateContentGbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('FRONTEND_MULTILANG') === true) {
            ContentTableCreateProcessor::create(ContentTableInterface::CONTENT_TABLE_PREFIX . 'gb');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('FRONTEND_MULTILANG') === true) {
            Schema::dropIfExists('content_gb');
        }
    }
}
