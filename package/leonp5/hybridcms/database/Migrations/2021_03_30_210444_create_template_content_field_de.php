<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\ContentField\Business\Processor\ContentFieldTableCreate\ContentFieldTableCreateProcessor;

class CreateTemplateContentFieldDe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ContentFieldTableCreateProcessor::createTable(
            ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . 'de'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . 'de');
    }
}
