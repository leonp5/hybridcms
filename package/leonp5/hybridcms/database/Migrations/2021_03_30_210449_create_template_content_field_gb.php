<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\ContentField\Business\Processor\ContentFieldTableCreate\ContentFieldTableCreateProcessor;

class CreateTemplateContentFieldGb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('FRONTEND_MULTILANG') === true) {
            ContentFieldTableCreateProcessor::createTable(
                ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . 'gb'
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('FRONTEND_MULTILANG') === true) {
            Schema::dropIfExists(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . 'gb',);
        }
    }
}
