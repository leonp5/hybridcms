<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Database\Seeder;

use Leonp5\Hybridcms\App\Models\ContentFieldType;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;

class ContentFieldTypeSeeder extends Seeder
{
    /**
     * 
     * @return void
     */
    public function run(): void
    {
        ContentFieldType::truncate();

        ContentFieldType::create([
            'id' => ContentTableInterface::CONTENT_TYPE_TEXT,
            'label' => 'hybridcms::pages.content.field.type.text'
        ]);
        ContentFieldType::create([
            'id' => ContentTableInterface::CONTENT_TYPE_EDITOR,
            'label' => 'hybridcms::pages.content.field.type.editor'
        ]);
    }
}
